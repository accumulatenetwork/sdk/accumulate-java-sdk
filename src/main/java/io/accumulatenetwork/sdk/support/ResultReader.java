package io.accumulatenetwork.sdk.support;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.accumulatenetwork.sdk.generated.apiv2.MultiResponse;
import io.accumulatenetwork.sdk.generated.apiv2.TxResponse;
import io.accumulatenetwork.sdk.generated.protocol.AccountType;
import io.accumulatenetwork.sdk.generated.protocol.TransactionStatus;
import io.accumulatenetwork.sdk.generated.protocol.TransactionType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResultReader {

    private static final ObjectMapper objectMapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_MISSING_EXTERNAL_TYPE_ID_PROPERTY, false)
            .registerModule(new JavaTimeModule());
    private static final ObjectReader objectReader = objectMapper.reader();


    public static <T> T readValue(final String json, final Class<?> valueType) {
        try {
            return (T) objectReader.readValue(json, valueType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T readValue(final JsonNode node, final Class<?> valueType) {
        return (T) objectMapper.convertValue(node, valueType);
    }

    public static <T> T readList(final JsonNode node) {
        return (T) objectMapper.convertValue(node, new TypeReference<List<T>>() {
        });
    }

    public static <T> io.accumulatenetwork.sdk.protocol.MultiResponse<T> readMultiResponse(final JsonNode node, final Class aClass) {
        final MultiResponse multiResponse = readValue(node, MultiResponse.class);

        final List<T> items = objectMapper.convertValue(multiResponse.getItems(),
                objectMapper.getTypeFactory().constructCollectionType(List.class, aClass));
        final List<JsonNode> otherItems = new ArrayList<>();
        if (multiResponse.getOtherItems() != null) {
            otherItems.addAll(Arrays.asList(multiResponse.getOtherItems()));
        }
        return new io.accumulatenetwork.sdk.protocol.MultiResponse(items, otherItems,
                multiResponse.getStart(), multiResponse.getCount(), multiResponse.getTotal());
    }

    public static void checkForErrors(final TxResponse txResponse) {
        if (txResponse == null) {
            throw new RuntimeException("No transaction response");
        }
        if (txResponse.getCode() != 0) {
            throw new RuntimeException(String.format("Transaction error: %d - %s", txResponse.getCode(), txResponse.getMessage()));
        }
    }

    public static void checkForErrors(final TxResponse txResponse, final TransactionStatus txStatus) {
        if (txResponse == null) {
            throw new RuntimeException("No transaction response");
        }
        if (txStatus == null) {
            checkForErrors(txResponse);
        }

        RuntimeException exception = null;
        final var error = txStatus.getError();
        if (error != null) {
            exception = new RuntimeException(String.format("Transaction status error: %d - %s", error.getCode().getValue(), error.getMessage()));
        }
        if (txResponse.getCode() != 0) {
            exception = new RuntimeException(String.format("Transaction error: %d - %s", txResponse.getCode(), txResponse.getMessage()), exception);
        }
        if (exception != null)
            throw exception;
    }

    public static TransactionType getTransactionType(final JsonNode jsonNode) {
        if (jsonNode.isTextual() || jsonNode.isNumber()) {
            return TransactionType.fromJsonNode(jsonNode);
        }
        if (jsonNode.isObject() && jsonNode.has("type")) {
            return TransactionType.fromJsonNode(jsonNode.get("type"));
        }
        if (jsonNode.isObject() && jsonNode.has("from") && jsonNode.has("to")) { // txType is missing when sending tokens between lite token accounts
            return TransactionType.SEND_TOKENS;
        }
        throw new RuntimeException(String.format("Can't determine a transaction type from '%s' ", jsonNode.toPrettyString()));
    }

    public static AccountType getAccountType(final JsonNode jsonNode) {
        if (jsonNode.isTextual() || jsonNode.isNumber()) {
            return AccountType.fromJsonNode(jsonNode);
        }
        if (jsonNode.isObject() && jsonNode.has("type")) {
            return AccountType.fromJsonNode(jsonNode.get("type"));
        }
        throw new RuntimeException(String.format("Can't determine a account type from '%s' ", jsonNode.toPrettyString()));
    }

}
