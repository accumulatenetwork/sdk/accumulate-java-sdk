package io.accumulatenetwork.sdk.generated.query;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: QueryType
// UnionValue: DataSet

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("RequestDataEntrySet")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDataEntrySet implements Marshallable {
	public final QueryType type = QueryType.DATA_SET;
	private Url url;
	private long start;
	private long count;
	private boolean expandChains;

    //
	public Url getUrl() {
	    return url;
	}
	public void setUrl(final Url value) {
	    this.url = value;
	}

	public RequestDataEntrySet url(final Url value) {
	    setUrl(value);
	    return this;
	}
	public RequestDataEntrySet url(final String value) {
	    setUrl(Url.toAccURL(value));
	    return this;
	}
	public long getStart() {
	    return start;
	}
	public void setStart(final long value) {
	    this.start = value;
	}

	public RequestDataEntrySet start(final long value) {
	    setStart(value);
	    return this;
	}
	public long getCount() {
	    return count;
	}
	public void setCount(final long value) {
	    this.count = value;
	}

	public RequestDataEntrySet count(final long value) {
	    setCount(value);
	    return this;
	}
	public boolean getExpandChains() {
	    return expandChains;
	}
	public void setExpandChains(final boolean value) {
	    this.expandChains = value;
	}

	public RequestDataEntrySet expandChains(final boolean value) {
	    setExpandChains(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        marshaller.writeValue(1, type);
        if (!(this.url == null)) {
            marshaller.writeUrl(2, this.url);
        }
        if (!(this.start == 0)) {
            marshaller.writeUint(3, this.start);
        }
        if (!(this.count == 0)) {
            marshaller.writeUint(4, this.count);
        }
        if (!(!this.expandChains)) {
            marshaller.writeBool(5, this.expandChains);
        }
        return marshaller.array();
    }
}
