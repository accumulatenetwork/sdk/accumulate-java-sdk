package io.accumulatenetwork.sdk.generated.apiv2;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.RPCBody;
import io.accumulatenetwork.sdk.protocol.Url;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("UrlQuery")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UrlQuery implements RPCBody {
	private Url url;

    //
	public Url getUrl() {
	    return url;
	}
	public void setUrl(final Url value) {
	    this.url = value;
	}

	public UrlQuery url(final Url value) {
	    setUrl(value);
	    return this;
	}
	public UrlQuery url(final String value) {
	    setUrl(Url.toAccURL(value));
	    return this;
	}
}
