package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.protocol.TxID;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("PartitionSyntheticLedger")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartitionSyntheticLedger implements Marshallable {
	private Url url;
	private long produced;
	private long received;
	private long delivered;
	private TxID[] pending;

    //
	public Url getUrl() {
	    return url;
	}
	public void setUrl(final Url value) {
	    this.url = value;
	}

	public PartitionSyntheticLedger url(final Url value) {
	    setUrl(value);
	    return this;
	}
	public PartitionSyntheticLedger url(final String value) {
	    setUrl(Url.toAccURL(value));
	    return this;
	}
	public long getProduced() {
	    return produced;
	}
	public void setProduced(final long value) {
	    this.produced = value;
	}

	public PartitionSyntheticLedger produced(final long value) {
	    setProduced(value);
	    return this;
	}
	public long getReceived() {
	    return received;
	}
	public void setReceived(final long value) {
	    this.received = value;
	}

	public PartitionSyntheticLedger received(final long value) {
	    setReceived(value);
	    return this;
	}
	public long getDelivered() {
	    return delivered;
	}
	public void setDelivered(final long value) {
	    this.delivered = value;
	}

	public PartitionSyntheticLedger delivered(final long value) {
	    setDelivered(value);
	    return this;
	}
	public TxID[] getPending() {
	    return pending;
	}
	public void setPending(final TxID[] value) {
	    this.pending = value;
	}

	public PartitionSyntheticLedger pending(final TxID[] value) {
	    setPending(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        if (!(this.url == null)) {
            marshaller.writeUrl(1, this.url);
        }
        if (!(this.produced == 0)) {
            marshaller.writeUint(2, this.produced);
        }
        if (!(this.received == 0)) {
            marshaller.writeUint(3, this.received);
        }
        if (!(this.delivered == 0)) {
            marshaller.writeUint(4, this.delivered);
        }
        if (!(this.pending == null || this.pending.length == 0)) {
            marshaller.writeTxid(5, this.pending);
        }
        return marshaller.array();
    }
}
