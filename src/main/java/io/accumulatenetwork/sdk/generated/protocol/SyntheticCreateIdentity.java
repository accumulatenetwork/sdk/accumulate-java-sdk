package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Account;
import io.accumulatenetwork.sdk.protocol.TransactionBody;
import io.accumulatenetwork.sdk.protocol.TxID;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: TransactionType
// UnionValue: SyntheticCreateIdentity

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("SyntheticCreateIdentity")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SyntheticCreateIdentity implements TransactionBody {
	public final TransactionType type = TransactionType.SYNTHETIC_CREATE_IDENTITY;
	private TxID cause;
	private Url source;
	private Url initiator;
	private long feeRefund;
	private Account[] accounts;

    //
	public TxID getCause() {
	    return cause;
	}
	public void setCause(final TxID value) {
	    this.cause = value;
	}

	public SyntheticCreateIdentity cause(final TxID value) {
	    setCause(value);
	    return this;
	}
	public Url getSource() {
	    return source;
	}
	public void setSource(final Url value) {
	    this.source = value;
	}

	public SyntheticCreateIdentity source(final Url value) {
	    setSource(value);
	    return this;
	}
	public SyntheticCreateIdentity source(final String value) {
	    setSource(Url.toAccURL(value));
	    return this;
	}
	public Url getInitiator() {
	    return initiator;
	}
	public void setInitiator(final Url value) {
	    this.initiator = value;
	}

	public SyntheticCreateIdentity initiator(final Url value) {
	    setInitiator(value);
	    return this;
	}
	public SyntheticCreateIdentity initiator(final String value) {
	    setInitiator(Url.toAccURL(value));
	    return this;
	}
	public long getFeeRefund() {
	    return feeRefund;
	}
	public void setFeeRefund(final long value) {
	    this.feeRefund = value;
	}

	public SyntheticCreateIdentity feeRefund(final long value) {
	    setFeeRefund(value);
	    return this;
	}
	public Account[] getAccounts() {
	    return accounts;
	}
	public void setAccounts(final Account[] value) {
	    this.accounts = value;
	}

	public SyntheticCreateIdentity accounts(final Account[] value) {
	    setAccounts(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        marshaller.writeValue(1, type);
        if (!(this.cause == null)) {
            marshaller.writeTxid(2, this.cause);
        }
        if (!(this.source == null)) {
            marshaller.writeUrl(3, this.source);
        }
        if (!(this.initiator == null)) {
            marshaller.writeUrl(4, this.initiator);
        }
        if (!(this.feeRefund == 0)) {
            marshaller.writeUint(5, this.feeRefund);
        }
        if (!(this.accounts == null)) {
            marshaller.writeValue(6, accounts);
        }
        return marshaller.array();
    }
}
