package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("PartitionInfo")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartitionInfo implements Marshallable {
	private String id;
	private PartitionType type;

    //
	public String getID() {
	    return id;
	}
	public void setID(final String value) {
	    this.id = value;
	}

	public PartitionInfo id(final String value) {
	    setID(value);
	    return this;
	}
	public PartitionType getType() {
	    return type;
	}
	public void setType(final PartitionType value) {
	    this.type = value;
	}

	public PartitionInfo type(final PartitionType value) {
	    setType(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        if (!(this.id == null || this.id.length() == 0)) {
            marshaller.writeString(1, this.id);
        }
        if (!(this.type == null)) {
            marshaller.writeValue(2, type);
        }
        return marshaller.array();
    }
}
