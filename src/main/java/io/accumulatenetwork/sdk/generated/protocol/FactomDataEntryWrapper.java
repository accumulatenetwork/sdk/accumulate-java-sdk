package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.accumulatenetwork.sdk.protocol.DataEntry;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: DataEntryType
// UnionValue: Factom

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("FactomDataEntryWrapper")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FactomDataEntryWrapper implements DataEntry {
	public final DataEntryType type = DataEntryType.FACTOM;
	@JsonUnwrapped
	private FactomDataEntry factomDataEntry;

    //
	public FactomDataEntry getFactomDataEntry() {
	    return factomDataEntry;
	}
	public void setFactomDataEntry(final FactomDataEntry value) {
	    this.factomDataEntry = value;
	}

	public FactomDataEntryWrapper factomDataEntry(final FactomDataEntry value) {
	    setFactomDataEntry(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        marshaller.writeValue(1, type);
        if (!(this.factomDataEntry == null)) {
            marshaller.writeValue(2, factomDataEntry);
        }
        return marshaller.array();
    }
}
