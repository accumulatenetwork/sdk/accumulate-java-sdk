package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Signature;
import io.accumulatenetwork.sdk.protocol.TxID;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: SignatureType
// UnionValue: ED25519

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("AuthoritySignature")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthoritySignature implements Signature {
	public final SignatureType type = SignatureType.AUTHORITY;
	private Url origin;
	private Url authority;
	private VoteType vote;
	private TxID txID;
	private TxID cause;
	private Url[] delegator;

	public SignatureType getType() {
		return type;
	}

	public Url getOrigin() {
		return origin;
	}

	public void setOrigin(Url origin) {
		this.origin = origin;
	}

	public Url getAuthority() {
		return authority;
	}

	public void setAuthority(Url authority) {
		this.authority = authority;
	}

	public VoteType getVote() {
		return vote;
	}

	public void setVote(VoteType vote) {
		this.vote = vote;
	}

	public TxID getTxID() {
		return txID;
	}

	public void setTxID(TxID txID) {
		this.txID = txID;
	}

	public TxID getCause() {
		return cause;
	}

	public void setCause(TxID cause) {
		this.cause = cause;
	}

	public Url[] getDelegator() {
		return delegator;
	}

	public void setDelegator(Url[] delegator) {
		this.delegator = delegator;
	}

	public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        marshaller.writeValue(1, type);
        if (!(this.origin == null)) {
            marshaller.writeUrl(2, this.origin);
        }
        if (!(this.authority == null)) {
            marshaller.writeUrl(3, this.authority);
        }
		if (!(this.vote == null)) {
			marshaller.writeValue(4, vote);
		}
		if (!(this.txID == null)) {
			marshaller.writeTxid(5, this.txID);
		}
		if (!(this.cause == null)) {
			marshaller.writeTxid(6, this.cause);
		}
		if (!(this.delegator == null)) {
			marshaller.writeUrl(7, this.delegator);
		}
        return marshaller.array();
    }
}
