package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Fee;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("FeeSchedule")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeeSchedule implements Marshallable {
	private Fee[] createIdentitySliding;

    //
	public Fee[] getCreateIdentitySliding() {
	    return createIdentitySliding;
	}
	public void setCreateIdentitySliding(final Fee[] value) {
	    this.createIdentitySliding = value;
	}

	public FeeSchedule createIdentitySliding(final Fee[] value) {
	    setCreateIdentitySliding(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        if (!(this.createIdentitySliding == null)) {
            marshaller.writeValue(1, createIdentitySliding);
        }
        return marshaller.array();
    }
}
