package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("NetworkLimits")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NetworkLimits implements Marshallable {
	private long dataEntryParts;
	private long accountAuthorities;
	private long bookPages;
	private long pageEntries;
	private long identityAccounts;

    //
	public long getDataEntryParts() {
	    return dataEntryParts;
	}
	public void setDataEntryParts(final long value) {
	    this.dataEntryParts = value;
	}

	public NetworkLimits dataEntryParts(final long value) {
	    setDataEntryParts(value);
	    return this;
	}
	public long getAccountAuthorities() {
	    return accountAuthorities;
	}
	public void setAccountAuthorities(final long value) {
	    this.accountAuthorities = value;
	}

	public NetworkLimits accountAuthorities(final long value) {
	    setAccountAuthorities(value);
	    return this;
	}
	public long getBookPages() {
	    return bookPages;
	}
	public void setBookPages(final long value) {
	    this.bookPages = value;
	}

	public NetworkLimits bookPages(final long value) {
	    setBookPages(value);
	    return this;
	}
	public long getPageEntries() {
	    return pageEntries;
	}
	public void setPageEntries(final long value) {
	    this.pageEntries = value;
	}

	public NetworkLimits pageEntries(final long value) {
	    setPageEntries(value);
	    return this;
	}
	public long getIdentityAccounts() {
	    return identityAccounts;
	}
	public void setIdentityAccounts(final long value) {
	    this.identityAccounts = value;
	}

	public NetworkLimits identityAccounts(final long value) {
	    setIdentityAccounts(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        if (!(this.dataEntryParts == 0)) {
            marshaller.writeUint(1, this.dataEntryParts);
        }
        if (!(this.accountAuthorities == 0)) {
            marshaller.writeUint(2, this.accountAuthorities);
        }
        if (!(this.bookPages == 0)) {
            marshaller.writeUint(3, this.bookPages);
        }
        if (!(this.pageEntries == 0)) {
            marshaller.writeUint(4, this.pageEntries);
        }
        if (!(this.identityAccounts == 0)) {
            marshaller.writeUint(5, this.identityAccounts);
        }
        return marshaller.array();
    }
}
