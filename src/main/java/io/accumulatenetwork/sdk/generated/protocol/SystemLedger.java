package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.accumulatenetwork.sdk.protocol.Account;
import io.accumulatenetwork.sdk.protocol.AnchorBody;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.support.Marshaller;
import io.accumulatenetwork.sdk.support.serializers.GoBigIntDeserializer;
import io.accumulatenetwork.sdk.support.serializers.GoBigIntSerializer;
// UnionType: AccountType
// UnionValue: SystemLedger

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("SystemLedger")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SystemLedger implements Account {
	public final AccountType type = AccountType.SYSTEM_LEDGER;
	private Url url;
	private long index;
	private java.time.OffsetDateTime timestamp;
	private java.math.BigInteger acmeBurnt;
	private NetworkAccountUpdate[] pendingUpdates;
	private AnchorBody anchor;

    //
	public Url getUrl() {
	    return url;
	}
	public void setUrl(final Url value) {
	    this.url = value;
	}

	public SystemLedger url(final Url value) {
	    setUrl(value);
	    return this;
	}
	public SystemLedger url(final String value) {
	    setUrl(Url.toAccURL(value));
	    return this;
	}
	public long getIndex() {
	    return index;
	}
	public void setIndex(final long value) {
	    this.index = value;
	}

	public SystemLedger index(final long value) {
	    setIndex(value);
	    return this;
	}
	public java.time.OffsetDateTime getTimestamp() {
	    return timestamp;
	}
	public void setTimestamp(final java.time.OffsetDateTime value) {
	    this.timestamp = value;
	}

	public SystemLedger timestamp(final java.time.OffsetDateTime value) {
	    setTimestamp(value);
	    return this;
	}
	@JsonDeserialize(using = GoBigIntDeserializer.class)
	public java.math.BigInteger getAcmeBurnt() {
	    return acmeBurnt;
	}
	@JsonSerialize(using = GoBigIntSerializer.class)
	public void setAcmeBurnt(final java.math.BigInteger value) {
	    this.acmeBurnt = value;
	}

	public SystemLedger acmeBurnt(final java.math.BigInteger value) {
	    setAcmeBurnt(value);
	    return this;
	}
	public NetworkAccountUpdate[] getPendingUpdates() {
	    return pendingUpdates;
	}
	public void setPendingUpdates(final NetworkAccountUpdate[] value) {
	    this.pendingUpdates = value;
	}

	public SystemLedger pendingUpdates(final NetworkAccountUpdate[] value) {
	    setPendingUpdates(value);
	    return this;
	}
	public AnchorBody getAnchor() {
	    return anchor;
	}
	public void setAnchor(final AnchorBody value) {
	    this.anchor = value;
	}

	public SystemLedger anchor(final AnchorBody value) {
	    setAnchor(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        marshaller.writeValue(1, type);
        if (!(this.url == null)) {
            marshaller.writeUrl(2, this.url);
        }
        if (!(this.index == 0)) {
            marshaller.writeUint(3, this.index);
        }
        if (!(this.timestamp == null)) {
            marshaller.writeTime(4, this.timestamp);
        }
        if (!((this.acmeBurnt).equals(java.math.BigInteger.ZERO))) {
            marshaller.writeBigInt(5, this.acmeBurnt);
        }
        if (!(this.pendingUpdates == null)) {
            marshaller.writeValue(6, pendingUpdates);
        }
        if (!(this.anchor == null)) {
            marshaller.writeValue(7, anchor);
        }
        return marshaller.array();
    }
}
