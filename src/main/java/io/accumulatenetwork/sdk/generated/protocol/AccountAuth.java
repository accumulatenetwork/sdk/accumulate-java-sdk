package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("AccountAuth")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountAuth implements Marshallable {
	private AuthorityEntry[] authorities;

    //
	public AuthorityEntry[] getAuthorities() {
	    return authorities;
	}
	public void setAuthorities(final AuthorityEntry[] value) {
	    this.authorities = value;
	}

	public AccountAuth authorities(final AuthorityEntry[] value) {
	    setAuthorities(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        if (!(this.authorities == null)) {
            marshaller.writeValue(1, authorities);
        }
        return marshaller.array();
    }
}
