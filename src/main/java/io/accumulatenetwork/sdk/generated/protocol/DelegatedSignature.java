package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Signature;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: SignatureType
// UnionValue: Delegated

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("DelegatedSignature")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DelegatedSignature implements Signature {
	public final SignatureType type = SignatureType.DELEGATED;
	private Signature signature;
	private Url delegator;

    //
	public Signature getSignature() {
	    return signature;
	}
	public void setSignature(final Signature value) {
	    this.signature = value;
	}

	public DelegatedSignature signature(final Signature value) {
	    setSignature(value);
	    return this;
	}
	public Url getDelegator() {
	    return delegator;
	}
	public void setDelegator(final Url value) {
	    this.delegator = value;
	}

	public DelegatedSignature delegator(final Url value) {
	    setDelegator(value);
	    return this;
	}
	public DelegatedSignature delegator(final String value) {
	    setDelegator(Url.toAccURL(value));
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        marshaller.writeValue(1, type);
        if (!(this.signature == null)) {
            marshaller.writeValue(2, signature);
        }
        if (!(this.delegator == null)) {
            marshaller.writeUrl(3, this.delegator);
        }
        return marshaller.array();
    }
}
