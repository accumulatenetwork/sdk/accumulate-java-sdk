package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("Object")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProtocolObject implements Marshallable {
	private ObjectType type;
	private ChainMetadata[] chains;
	private TxIdSet pending;

    //
	public ObjectType getType() {
	    return type;
	}
	public void setType(final ObjectType value) {
	    this.type = value;
	}

	public Object type(final ObjectType value) {
	    setType(value);
	    return this;
	}
	public ChainMetadata[] getChains() {
	    return chains;
	}
	public void setChains(final ChainMetadata[] value) {
	    this.chains = value;
	}

	public Object chains(final ChainMetadata[] value) {
	    setChains(value);
	    return this;
	}
	public TxIdSet getPending() {
	    return pending;
	}
	public void setPending(final TxIdSet value) {
	    this.pending = value;
	}

	public Object pending(final TxIdSet value) {
	    setPending(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        if (!(this.type == null)) {
            marshaller.writeValue(1, type);
        }
        if (!(this.chains == null)) {
            marshaller.writeValue(2, chains);
        }
        if (!(this.pending == null)) {
            marshaller.writeValue(3, pending);
        }
        return marshaller.array();
    }
}
