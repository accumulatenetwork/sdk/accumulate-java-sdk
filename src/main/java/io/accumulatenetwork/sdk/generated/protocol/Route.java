package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.accumulatenetwork.sdk.protocol.Marshallable;
import io.accumulatenetwork.sdk.support.Marshaller;
// UnionType: 
// UnionValue: 

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("Route")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Route implements Marshallable {
	private long length;
	private long value;
	private String partition;

    //
	public long getLength() {
	    return length;
	}
	public void setLength(final long value) {
	    this.length = value;
	}

	public Route length(final long value) {
	    setLength(value);
	    return this;
	}
	public long getValue() {
	    return value;
	}
	public void setValue(final long value) {
	    this.value = value;
	}

	public Route value(final long value) {
	    setValue(value);
	    return this;
	}
	public String getPartition() {
	    return partition;
	}
	public void setPartition(final String value) {
	    this.partition = value;
	}

	public Route partition(final String value) {
	    setPartition(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        if (!(this.length == 0)) {
            marshaller.writeUint(1, this.length);
        }
        if (!(this.value == 0)) {
            marshaller.writeUint(2, this.value);
        }
        if (!(this.partition == null || this.partition.length() == 0)) {
            marshaller.writeString(3, this.partition);
        }
        return marshaller.array();
    }
}
