package io.accumulatenetwork.sdk.generated.protocol;

/**
    GENERATED BY go run ./tools/cmd/gen-api. DO NOT EDIT.
**/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.accumulatenetwork.sdk.protocol.TransactionBody;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.support.Marshaller;
import io.accumulatenetwork.sdk.support.serializers.HexDeserializer;
import io.accumulatenetwork.sdk.support.serializers.HexSerializer;
// UnionType: TransactionType
// UnionValue: CreateIdentity

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("CreateIdentity")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateIdentity implements TransactionBody {
	public final TransactionType type = TransactionType.CREATE_IDENTITY;
	private Url url;
	private byte[] keyHash;
	private Url keyBookUrl;
	private Url[] authorities;

    //
	public Url getUrl() {
	    return url;
	}
	public void setUrl(final Url value) {
	    this.url = value;
	}

	public CreateIdentity url(final Url value) {
	    setUrl(value);
	    return this;
	}
	public CreateIdentity url(final String value) {
	    setUrl(Url.toAccURL(value));
	    return this;
	}
	@JsonDeserialize(using = HexDeserializer.class)
	public byte[] getKeyHash() {
	    return keyHash;
	}
	@JsonSerialize(using = HexSerializer.class)
	public void setKeyHash(final byte[] value) {
	    this.keyHash = value;
	}

	public CreateIdentity keyHash(final byte[] value) {
	    setKeyHash(value);
	    return this;
	}
	public CreateIdentity keyHash(final String value) {
		try {
			setKeyHash(io.accumulatenetwork.sdk.commons.codec.binary.Hex.decodeHex(value));
		} catch (io.accumulatenetwork.sdk.commons.codec.DecoderException e) {
			throw new RuntimeException(e);
		}
	    return this;
	}
	public Url getKeyBookUrl() {
	    return keyBookUrl;
	}
	public void setKeyBookUrl(final Url value) {
	    this.keyBookUrl = value;
	}

	public CreateIdentity keyBookUrl(final Url value) {
	    setKeyBookUrl(value);
	    return this;
	}
	public CreateIdentity keyBookUrl(final String value) {
	    setKeyBookUrl(Url.toAccURL(value));
	    return this;
	}
	public Url[] getAuthorities() {
	    return authorities;
	}
	public void setAuthorities(final Url[] value) {
	    this.authorities = value;
	}

	public CreateIdentity authorities(final Url[] value) {
	    setAuthorities(value);
	    return this;
	}
    public byte[] marshalBinary() {
        final var marshaller = new Marshaller();
        marshaller.writeValue(1, type);
        if (!(this.url == null)) {
            marshaller.writeUrl(2, this.url);
        }
        if (!(this.keyHash == null || this.keyHash.length == 0)) {
            marshaller.writeBytes(3, this.keyHash);
        }
        if (!(this.keyBookUrl == null)) {
            marshaller.writeUrl(4, this.keyBookUrl);
        }
        if (!(this.authorities == null || this.authorities.length == 0)) {
            marshaller.writeUrl(6, this.authorities);
        }
        return marshaller.array();
    }
}
