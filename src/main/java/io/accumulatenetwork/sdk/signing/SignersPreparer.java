package io.accumulatenetwork.sdk.signing;

import io.accumulatenetwork.sdk.protocol.Principal;
import io.accumulatenetwork.sdk.protocol.SignatureKeyPair;
import io.accumulatenetwork.sdk.protocol.Url;

import java.util.ArrayList;
import java.util.List;

public class SignersPreparer {

    private final Url signerUrl;
    private final int signerVersion;

    private SignatureKeyPair signatureKeyPair;
    private final List<Principal> additionalSignerSigners = new ArrayList<>();
    private final List<Url> delegators = new ArrayList<>();


    public SignersPreparer(final SignatureKeyPair signatureKeyPair, final Url signerUrl, final int signerVersion) {
        this.signerUrl = signerUrl;
        this.signatureKeyPair = signatureKeyPair;
        this.signerVersion = signerVersion;
    }


    public SignersPreparer withDelegators(final List<Url> delegators) {
        delegators.addAll(delegators);
        return this;
    }

    public SignersPreparer withAdditionalSigners(final List<Principal> additionalSigners) {
        this.additionalSignerSigners.addAll(additionalSigners);
        return this;
    }

    public SignersPreparer withAdditionalSigner(final Principal additionalSigner) {
        this.additionalSignerSigners.add(additionalSigner);
        return this;
    }


    public List<Signer> prepareSigners() {
        final List<Signer> signers = new ArrayList<>();
        final var firstSigner = new Signer()
                .withNonceFromTimeNow();
        firstSigner.withDelegators(delegators);
        firstSigner.withType(getSignatureKeyPair().getSignatureType())
                .withUrl(signerUrl)
                .withVersion(signerVersion)
                .withSignerPrivateKey(getSignatureKeyPair().getPrivateKey());
        signers.add(firstSigner);

        additionalSignerSigners.forEach(principal -> {

            final var signer = new Signer()
                    .withDelegators(delegators)
                    .withType(principal.getSignatureKeyPair().getSignatureType())
                    .withUrl(principal.getAccount().getUrl())
                    .withVersion(principal.getSignerVersion())
                    .withSignerPrivateKey(principal.getSignatureKeyPair().getPrivateKey());
            signers.add(signer);
        });
        return signers;
    }


    public Url getSignerUrl() {
        return signerUrl;
    }

    public SignatureKeyPair getSignatureKeyPair() {
        return signatureKeyPair;
    }

    public List<Url> getDelegators() {
        return delegators;
    }

    public List<Principal> getAdditionalSigners() {
        return additionalSignerSigners;
    }
}
