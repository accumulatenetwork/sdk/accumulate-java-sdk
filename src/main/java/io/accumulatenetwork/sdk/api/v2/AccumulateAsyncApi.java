package io.accumulatenetwork.sdk.api.v2;

import com.fasterxml.jackson.databind.JsonNode;
import io.accumulatenetwork.sdk.commons.codec.binary.Hex;
import io.accumulatenetwork.sdk.generated.apiv2.*;
import io.accumulatenetwork.sdk.generated.protocol.*;
import io.accumulatenetwork.sdk.generated.query.ResponseDataEntry;
import io.accumulatenetwork.sdk.generated.query.ResponseKeyPageIndex;
import io.accumulatenetwork.sdk.protocol.MultiResponse;
import io.accumulatenetwork.sdk.protocol.*;
import io.accumulatenetwork.sdk.rpc.AsyncRPCClient;
import io.accumulatenetwork.sdk.rpc.RPCException;
import io.accumulatenetwork.sdk.signing.SignersPreparer;
import io.accumulatenetwork.sdk.support.ResultReader;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@SuppressWarnings("unused")
public class AccumulateAsyncApi {

    private final AsyncRPCClient rpcClient;

    private DescriptionResponse descriptionResponse;

    protected Long oraclePrice;

    public AccumulateAsyncApi() {
        rpcClient = new AsyncRPCClient();
    }

    public AccumulateAsyncApi(final URI uri) {
        rpcClient = new AsyncRPCClient(uri);
    }

    public CompletableFuture<TransactionResult<EmptyResult>> faucet(final Url url) {
        return rpcClient.sendTx(new AcmeFaucet().url(url))
                .thenApply(txResponse -> new TransactionResult<>(txResponse.getTxid(), new EmptyResult()));
    }

    public CompletableFuture<DescriptionResponse> describe() {
        return rpcClient.send(RPCMethod.Describe, null).thenApply(rpcResponse ->
        {
            this.descriptionResponse = ResultReader.readValue(rpcResponse.getResultNode(), DescriptionResponse.class);
            return descriptionResponse;
        });
    }

    public CompletableFuture<TransactionResult<AddCreditsResult>> addCredits(final AddCredits addCredits, final Principal... signers) {
        CompletableFuture<Long> futureOraclePrice = getOraclePrice(addCredits.getOracle());
        return futureOraclePrice.thenCompose(oraclePrice -> {
            addCredits.setOracle(oraclePrice);
            return envelopeBuilder(addCredits, signers)
                    .thenCompose(envelopeBuilder -> rpcClient.sendTx(envelopeBuilder)
                            .thenApply(txStatus -> {
                                AddCreditsResult addCreditsResult;
                                io.accumulatenetwork.sdk.protocol.TransactionResult result = txStatus.getResult();
                                if (result == null || result instanceof EmptyResult) {
                                    addCreditsResult = new AddCreditsResult()
                                            .amount(addCredits.getAmount())
                                            .credits(addCredits.getAmount().longValue()) // This is not accurate, but it's all we have atm
                                            .oracle(oraclePrice);
                                } else {
                                    addCreditsResult = (AddCreditsResult) result;
                                }
                                return new TransactionResult<>(txStatus.getTxID(), addCreditsResult);
                            })
                            .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
        });
    }

    public CompletableFuture<Long> getOraclePrice() {
        return getOraclePrice(0);
    }

    private CompletableFuture<Long> getOraclePrice(final long givenPrice) {
        if (givenPrice > 0) {
            return CompletableFuture.supplyAsync(() -> givenPrice);
        } else if (this.oraclePrice != null) {
            return CompletableFuture.supplyAsync(() -> this.oraclePrice);
        } else {
            return describe().thenApply(descriptionResponse -> {
                this.oraclePrice = descriptionResponse.getValues().getOracle().getPrice();
                return oraclePrice;
            });
        }
    }

    public CompletableFuture<TransactionResult<EmptyResult>> createIdentity(final CreateIdentity createIdentity, final Principal... signers) {
        return sendTxNoResult(createIdentity, signers);
    }


    public CompletableFuture<TransactionResult<EmptyResult>> burnTokens(final BurnTokens burnTokens, final Principal... signers) {
        return sendTxNoResult(burnTokens, signers);
    }

    public CompletableFuture<TransactionResult<EmptyResult>> createDataAccount(final CreateDataAccount createDataAccount, final Principal... signers) {
        return sendTxNoResult(createDataAccount, signers);
    }

    public CompletableFuture<TxID> createTokenAccount(final CreateTokenAccount createTokenAccount, final Principal... signers) {
        return sendTxNoResult(createTokenAccount, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TxID> createLiteTokenAccount(final CreateLiteTokenAccount createLiteTokenAccount, final Principal... signers) {
        return sendTxNoResult(createLiteTokenAccount, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TransactionResult<WriteDataResult>> createLiteDataAccount(final FactomEntry factomEntry, final Principal... signers) {
        final byte[] accountId = factomEntry.calculateChainId();
        return writeFactomData(Hex.encodeHexString(accountId), factomEntry, signers);
    }

    public CompletableFuture<TransactionResult<WriteDataResult>> writeFactomData(final String chainId, final FactomEntry factomEntry, final Principal... signers) {
        final var writeDataTo = new WriteDataTo()
                .entry(new FactomDataEntryWrapper()
                        .factomDataEntry(new FactomDataEntry()
                                .accountId(chainId)
                                .extIds(factomExtRefsToByteArray(factomEntry.getExtRefs()))
                                .data(factomEntry.getData())
                        ))
                .recipient(Url.toAccURL(chainId));
        return envelopeBuilder(writeDataTo, signers)
                .thenCompose(envelopeBuilder -> rpcClient.sendTx(envelopeBuilder)
                        .thenApply(txStatus -> new TransactionResult<>(txStatus.getTxID(), (WriteDataResult) txStatus.getResult()))
                        .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    public CompletableFuture<TxID> createToken(final CreateToken createToken, final Principal... signers) {
        return sendTxNoResult(createToken, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TransactionResult<WriteDataResult>> writeData(final WriteData writeData, final Principal... signers) {
        return envelopeBuilder(writeData, signers)
                .thenCompose(envelopeBuilder -> rpcClient.sendTx(envelopeBuilder)
                        .thenApply(txStatus -> new TransactionResult<>(txStatus.getTxID(), (WriteDataResult) txStatus.getResult()))
                        .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    public CompletableFuture<TransactionResult<WriteDataResult>> writeDataTo(final WriteDataTo writeDataTo, final Principal... signers) {
        return envelopeBuilder(writeDataTo, signers)
                .thenCompose(envelopeBuilder -> rpcClient.sendTx(envelopeBuilder)
                        .thenApply(txStatus -> new TransactionResult<>(txStatus.getTxID(), (WriteDataResult) txStatus.getResult()))
                        .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    public CompletableFuture<TxID> issueTokens(final IssueTokens issueTokens, final Principal... signers) {
        return sendTxNoResult(issueTokens, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TxID> sendTokens(final SendTokens sendTokens, final Principal... signers) {
        return sendTxNoResult(sendTokens, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TxID> createKeyBook(final CreateKeyBook createKeyBook, final Principal... signers) {
        return sendTxNoResult(createKeyBook, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TxID> createKeyPage(final CreateKeyPage createKeyPage, final Principal... signers) {
        return sendTxNoResult(createKeyPage, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TxID> updateKeyPage(final UpdateKeyPage updateKeyPage, final Principal... signers) {
        return sendTxNoResult(updateKeyPage, signers)
                .thenApply(TransactionResult::getTxID);
    }

    public CompletableFuture<TxID> updateKey(final UpdateKey updateKey, final Principal... signers) {
        return sendTxNoResult(updateKey, signers)
                .thenApply(TransactionResult::getTxID);
    }

    private CompletableFuture<TransactionResult<EmptyResult>> sendTxNoResult(final TransactionBody transactionBody, final Principal... principal) {
        return envelopeBuilder(transactionBody, principal)
                .thenCompose(envelopeBuilder -> {
                    final CompletableFuture<TransactionResult<EmptyResult>> future = rpcClient.sendTx(envelopeBuilder)
                            .thenApply(txResponse -> new TransactionResult<>(txResponse.getTxID(), null));
                    future.whenComplete((emptyResultTransactionResult, throwable) -> envelopeBuilder.releasePrincipal());
                    return future;
                });
    }

    public CompletableFuture<TransactionQueryResult> getTx(final TxID txId) {
        return getTx(txId.getHash());
    }

    public CompletableFuture<TransactionQueryResult> getTx(final byte[] txId) {
        return getTx(new TxnQuery().txid(txId));
    }

    public CompletableFuture<TransactionQueryResult> getTx(final TxnQuery txnQuery) {
        return rpcClient.send(txnQuery)
                .thenApply(rpcResponse -> {
                    final TransactionQueryResponse txQueryResponse = ResultReader.readValue(rpcResponse.getResultNode(), TransactionQueryResponse.class);
                    return new TransactionQueryResult(txQueryResponse, TransactionType.fromApiName(txQueryResponse.getType()));
                });
    }

    public CompletableFuture<List<TransactionQueryResult>> getTxHistory(final TxHistoryQuery query) {
        try {
            return rpcClient.send(query)
                    .thenApply(rpcResponse -> {
                        final io.accumulatenetwork.sdk.generated.apiv2.MultiResponse multiResponse = ResultReader.readValue(rpcResponse.getResultNode(), io.accumulatenetwork.sdk.generated.apiv2.MultiResponse.class);
                        final List<TransactionQueryResult> result = new ArrayList<>((int) multiResponse.getCount());
                        if(multiResponse.getCount() > 0 && multiResponse.getItems() != null) {
                            for (final JsonNode item : multiResponse.getItems()) {
                                final TransactionQueryResponse txQueryResponse = ResultReader.readValue(item, TransactionQueryResponse.class);
                                result.add(new TransactionQueryResult(txQueryResponse, ResultReader.getTransactionType(txQueryResponse.getData())));
                            }
                        }
                        return result;
                    });
        } catch (final RPCException e) {
            throw new RuntimeException(e);
        }
    }


    public CompletableFuture<ResponseKeyPageIndex> queryKeyIndex(final KeyPageIndexQuery query) {
        return rpcClient.send(query).thenApply(rpcResponse -> {
            final ChainQueryResponse chainQueryResponse = ResultReader.readValue(rpcResponse.getResultNode(), ChainQueryResponse.class);
            final QueryResponseType queryResponseType = QueryResponseType.fromJsonNode(rpcResponse.getResultNode());
            if (queryResponseType != QueryResponseType.KEY_PAGE_INDEX) {
                throw new RuntimeException(String.format("Invalid query response type, expected %s got %s",
                        QueryResponseType.KEY_PAGE_INDEX.getResponseType(), queryResponseType.getResponseType()));
            }
            return ResultReader.readValue(chainQueryResponse.getData(), ResponseKeyPageIndex.class);
        });
    }

    public CompletableFuture<DataEntryQueryResponse> queryData(final DataEntryQuery query) {
        try {
            return rpcClient.send(query)
                    .thenApply(rpcResponse -> {
                        final ChainQueryResponse chainQueryResponse = ResultReader.readValue(rpcResponse.getResultNode(), ChainQueryResponse.class);
                        return ResultReader.readValue(chainQueryResponse.getData(), DataEntryQueryResponse.class);
                    });
        } catch (final RPCException e) {
            throw new RuntimeException(e);
        }
    }

    public CompletableFuture<MultiResponse<ResponseDataEntry>> queryData(final DataEntrySetQuery query) {
        try {
            return rpcClient.send(query)
                    .thenApply(rpcResponse ->
                            ResultReader.readMultiResponse(rpcResponse.getResultNode(), ResponseDataEntry.class));
        } catch (final RPCException e) {
            throw new RuntimeException(e);
        }
    }

    public CompletableFuture<ChainQueryResponse> queryUrl(final GeneralQuery generalQuery) {
        try {
            return rpcClient.send(generalQuery)
                    .thenApply(rpcResponse ->
                            ResultReader.readValue(rpcResponse.getResultNode(), ChainQueryResponse.class));
        } catch (final RPCException e) {
            throw new RuntimeException(e);
        }
    }

    public CompletableFuture<ChainQueryResponse> queryChain(final ChainIdQuery chainIdQuery) {
        return rpcClient.send(chainIdQuery)
                .thenApply(rpcResponse ->
                        ResultReader.readValue(rpcResponse.getResultNode(), ChainQueryResponse.class));
    }


    public CompletableFuture<MultiResponse<MajorQueryResponse>> queryMajorBlocks(final MajorBlocksQuery majorBlocksQuery) {
        return rpcClient.send(majorBlocksQuery)
                .thenApply(rpcResponse ->
                        ResultReader.readMultiResponse(rpcResponse.getResultNode(), MajorQueryResponse.class));
    }

    public CompletableFuture<MultiResponse<MinorQueryResponse>> queryMinorBlocks(final MinorBlocksQuery minorBlocksQuery) {
        return rpcClient.send(minorBlocksQuery)
                .thenApply(rpcResponse ->
                        ResultReader.readMultiResponse(rpcResponse.getResultNode(), MinorQueryResponse.class)
                );
    }

    public CompletableFuture<DataEntryQueryResponse> queryDataSet(final DataEntrySetQuery dataEntrySetQuery) {
        return rpcClient.send(dataEntrySetQuery)
                .thenApply(rpcResponse ->
                        ResultReader.readValue(rpcResponse.getResultNode(), DataEntryQueryResponse.class));
    }

    public CompletableFuture<MultiResponse<Url>> queryADIDirectory(final DirectoryQuery dataEntrySetQuery) {
        return rpcClient.send(dataEntrySetQuery)
                .thenApply(rpcResponse -> ResultReader.readMultiResponse(rpcResponse.getResultNode(), Url.class));
    }

    public CompletableFuture<TxID> addAuthority(final AddAccountAuthorityOperation addAccountAuthorityOperation, final Principal... signers) {
        AccountAuthOperation[] accountAuthOperations = {addAccountAuthorityOperation};
        UpdateAccountAuth updateAccountAuth = new UpdateAccountAuth()
                .operations(accountAuthOperations);
        return envelopeBuilder(updateAccountAuth, signers)
                .thenCompose(envelopeBuilder -> rpcClient.sendTx(envelopeBuilder)
                        .thenApply(TransactionStatus::getTxID)
                        .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    public CompletableFuture<TxID> disableAuthority(final DisableAccountAuthOperation disableAccountAuthOperation, final Principal... signers) {
        final AccountAuthOperation[] accountAuthOperations = {disableAccountAuthOperation};
        final UpdateAccountAuth updateAccountAuth = new UpdateAccountAuth()
                .operations(accountAuthOperations);
        return envelopeBuilder(updateAccountAuth, signers)
                .thenCompose(envelopeBuilder ->
                        rpcClient.sendTx(envelopeBuilder)
                                .thenApply(TransactionStatus::getTxID)
                                .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    public CompletableFuture<TxID> removeAuthority(final RemoveAccountAuthorityOperation removeAccountAuthorityOperation, final Principal... signers) {
        AccountAuthOperation[] accountAuthOperations = {removeAccountAuthorityOperation};
        UpdateAccountAuth updateAccountAuth = new UpdateAccountAuth()
                .operations(accountAuthOperations);
        return envelopeBuilder(updateAccountAuth, signers)
                .thenCompose(envelopeBuilder ->
                        rpcClient.sendTx(envelopeBuilder)
                                .thenApply(TransactionStatus::getTxID)
                                .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    public CompletableFuture<TxID> enableAuthority(final EnableAccountAuthOperation enableAccountAuthOperation, final Principal... signers) {
        AccountAuthOperation[] accountAuthOperations = {enableAccountAuthOperation};
        UpdateAccountAuth updateAccountAuth = new UpdateAccountAuth()
                .operations(accountAuthOperations);
        return envelopeBuilder(updateAccountAuth, signers)
                .thenCompose(envelopeBuilder ->
                        rpcClient.sendTx(envelopeBuilder)
                                .thenApply(transactionStatus -> {
                                    envelopeBuilder.releasePrincipal();
                                    return transactionStatus.getTxID();
                                })
                                .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    public CompletableFuture<TxID> signTx(final Transaction tx, final Principal... signers) {
        return envelopeBuilder(tx, signers)
                .thenCompose(envelopeBuilder ->
                        rpcClient.sendTx(envelopeBuilder)
                                .thenApply(transactionStatus -> {
                                    envelopeBuilder.releasePrincipal();
                                    return transactionStatus.getTxID();
                                })
                                .whenComplete((txID, throwable) -> envelopeBuilder.releasePrincipal()));
    }

    private CompletableFuture<EnvelopeBuilder> envelopeBuilder(final TransactionBody txBody, final Principal... signers) {
        if (signers.length == 0 || signers[0] == null) {
            throw new IllegalArgumentException("A minimum of 1 signer key is required");
        }

        return CompletableFuture.supplyAsync(() -> {
            final Principal initialSigner = signers[0];
            final var signersPreparer = new SignersPreparer(initialSigner.getSignatureKeyPair(), getSignerUrl(initialSigner),
                    initialSigner.getSignerVersion());
            for (int i = 1; i < signers.length; i++) {
                signersPreparer.withAdditionalSigner(signers[i]);
            }
            return new EnvelopeBuilder(initialSigner.getAccount().getUrl(), signersPreparer, txBody);
        });
    }

    private CompletableFuture<EnvelopeBuilder> envelopeBuilder(final Transaction tx, final Principal... signers) {
        if (signers.length == 0) {
            throw new IllegalArgumentException("A minimum of 1 signer key is required");
        }

        return CompletableFuture.supplyAsync(() -> {
            final Principal initialSigner = signers[0];
            final var signersPreparer = new SignersPreparer(initialSigner.getSignatureKeyPair(), getSignerUrl(initialSigner),
                    initialSigner.getSignerVersion());
            for (int i = 1; i < signers.length; i++) {
                signersPreparer.withAdditionalSigner(signers[i]);
            }
            return new EnvelopeBuilder(initialSigner.getAccount().getUrl(), signersPreparer, tx);
        });
    }

    private Url getSignerUrl(final Principal principal) {
        final var account = principal.getAccount();
        final var accountUrl = account.getUrl();
        if (!(account instanceof LiteIdentity) && !(account instanceof LiteTokenAccount)) {
            return queryKeyIndex(new KeyPageIndexQuery()
                    .url(accountUrl)
                    .key(principal.getSignatureKeyPair().getPublicKey())).join().getSigner();
        }
        return accountUrl;
    }


    private byte[][] factomExtRefsToByteArray(final List<FactomExtRef> extRefs) {
        int maxLen = 0;
        for (FactomExtRef extRef : extRefs) {
            if (extRef.getData() != null && extRef.getData().length > maxLen) {
                maxLen = extRef.getData().length;
            }
        }
        final byte[][] result = new byte[extRefs.size()][maxLen];
        for (int i = 0; i < extRefs.size(); i++) {
            final FactomExtRef extRef = extRefs.get(i);
            if (extRef != null) {
                result[i] = extRef.getData();
            }
        }
        return result;
    }
}
