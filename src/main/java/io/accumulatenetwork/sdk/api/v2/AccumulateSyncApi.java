package io.accumulatenetwork.sdk.api.v2;

/*
  TODO this package should just use AccumulateAsyncApi and return that with .join()
 */

import io.accumulatenetwork.sdk.generated.apiv2.ChainIdQuery;
import io.accumulatenetwork.sdk.generated.apiv2.ChainQueryResponse;
import io.accumulatenetwork.sdk.generated.apiv2.DataEntryQuery;
import io.accumulatenetwork.sdk.generated.apiv2.DataEntryQueryResponse;
import io.accumulatenetwork.sdk.generated.apiv2.DataEntrySetQuery;
import io.accumulatenetwork.sdk.generated.apiv2.DescriptionResponse;
import io.accumulatenetwork.sdk.generated.apiv2.DirectoryQuery;
import io.accumulatenetwork.sdk.generated.apiv2.GeneralQuery;
import io.accumulatenetwork.sdk.generated.apiv2.KeyPageIndexQuery;
import io.accumulatenetwork.sdk.generated.apiv2.MajorBlocksQuery;
import io.accumulatenetwork.sdk.generated.apiv2.MajorQueryResponse;
import io.accumulatenetwork.sdk.generated.apiv2.MinorBlocksQuery;
import io.accumulatenetwork.sdk.generated.apiv2.MinorQueryResponse;
import io.accumulatenetwork.sdk.generated.apiv2.TxHistoryQuery;
import io.accumulatenetwork.sdk.generated.apiv2.TxnQuery;
import io.accumulatenetwork.sdk.generated.protocol.AddAccountAuthorityOperation;
import io.accumulatenetwork.sdk.generated.protocol.AddCredits;
import io.accumulatenetwork.sdk.generated.protocol.AddCreditsResult;
import io.accumulatenetwork.sdk.generated.protocol.BurnTokens;
import io.accumulatenetwork.sdk.generated.protocol.CreateDataAccount;
import io.accumulatenetwork.sdk.generated.protocol.CreateIdentity;
import io.accumulatenetwork.sdk.generated.protocol.CreateKeyBook;
import io.accumulatenetwork.sdk.generated.protocol.CreateKeyPage;
import io.accumulatenetwork.sdk.generated.protocol.CreateLiteTokenAccount;
import io.accumulatenetwork.sdk.generated.protocol.CreateToken;
import io.accumulatenetwork.sdk.generated.protocol.CreateTokenAccount;
import io.accumulatenetwork.sdk.generated.protocol.DisableAccountAuthOperation;
import io.accumulatenetwork.sdk.generated.protocol.EnableAccountAuthOperation;
import io.accumulatenetwork.sdk.generated.protocol.IssueTokens;
import io.accumulatenetwork.sdk.generated.protocol.RemoveAccountAuthorityOperation;
import io.accumulatenetwork.sdk.generated.protocol.SendTokens;
import io.accumulatenetwork.sdk.generated.protocol.Transaction;
import io.accumulatenetwork.sdk.generated.protocol.UpdateKey;
import io.accumulatenetwork.sdk.generated.protocol.UpdateKeyPage;
import io.accumulatenetwork.sdk.generated.protocol.WriteData;
import io.accumulatenetwork.sdk.generated.protocol.WriteDataResult;
import io.accumulatenetwork.sdk.generated.protocol.WriteDataTo;
import io.accumulatenetwork.sdk.generated.query.ResponseDataEntry;
import io.accumulatenetwork.sdk.generated.query.ResponseKeyPageIndex;
import io.accumulatenetwork.sdk.protocol.FactomEntry;
import io.accumulatenetwork.sdk.protocol.MultiResponse;
import io.accumulatenetwork.sdk.protocol.Principal;
import io.accumulatenetwork.sdk.protocol.TxID;
import io.accumulatenetwork.sdk.protocol.Url;

import java.net.URI;
import java.util.List;
import java.util.concurrent.CompletionException;

@SuppressWarnings("unused")
public class AccumulateSyncApi {

    private final AccumulateAsyncApi asyncApi;

    public AccumulateSyncApi() {
        asyncApi = new AccumulateAsyncApi();
    }

    public AccumulateSyncApi(final URI uri) {
        asyncApi = new AccumulateAsyncApi(uri);
    }

    public TxID faucet(final Url url) {
        try {
            return asyncApi.faucet(url).join().getTxID();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    private RuntimeException convertException(final CompletionException e) {
        if (e.getCause() == null) {
            throw e;
        }

        if (e.getCause() instanceof RuntimeException) {
            throw (RuntimeException) e.getCause();
        }
        throw new RuntimeException(e.getCause());

    }

    public DescriptionResponse describe() {
        try {
            return asyncApi.describe().join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TransactionResult<AddCreditsResult> addCredits(final AddCredits addCredits, final Principal... signers) {
        try {
            return asyncApi.addCredits(addCredits, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID createIdentity(final CreateIdentity createIdentity, final Principal... signers) {
        try {
            return asyncApi.createIdentity(createIdentity, signers).join().getTxID();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID burnTokens(final BurnTokens burnTokens, final Principal... signers) {
        try {
            return asyncApi.burnTokens(burnTokens, signers).join().getTxID();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID createDataAccount(final CreateDataAccount createDataAccount, final Principal... signers) {
        try {
            return asyncApi.createDataAccount(createDataAccount, signers).join().getTxID();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID createTokenAccount(final CreateTokenAccount createTokenAccount, final Principal... signers) {
        try {
            return asyncApi.createTokenAccount(createTokenAccount, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID createLiteTokenAccount(final CreateLiteTokenAccount createLiteTokenAccount, final Principal... signers) {
        try {
            return asyncApi.createLiteTokenAccount(createLiteTokenAccount, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TransactionResult<WriteDataResult> createLiteDataAccount(final FactomEntry factomEntry, final Principal... signers) {
        try {
            return asyncApi.createLiteDataAccount(factomEntry, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID createToken(final CreateToken createToken, final Principal... signers) {
        try {
            return asyncApi.createToken(createToken, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TransactionResult<WriteDataResult> writeData(final WriteData writeData, final Principal... signers) {
        try {
            return asyncApi.writeData(writeData, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TransactionResult<WriteDataResult> writeDataTo(final WriteDataTo writeDataTo, final Principal... signers) {
        try {
            return asyncApi.writeDataTo(writeDataTo, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TransactionResult<WriteDataResult> writeFactomData(final String chainId, final FactomEntry factomEntry, final Principal... signers) {
        try {
            return asyncApi.writeFactomData(chainId, factomEntry, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID issueTokens(final IssueTokens issueTokens, final Principal... signers) {
        try {
            return asyncApi.issueTokens(issueTokens, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID sendTokens(final SendTokens sendTokens, final Principal... signers) {
        try {
            return asyncApi.sendTokens(sendTokens, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID createKeyBook(final CreateKeyBook createKeyBook, final Principal... signers) {
        try {
            return asyncApi.createKeyBook(createKeyBook, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID createKeyPage(final CreateKeyPage createKeyPage, final Principal... signers) {
        try {
            return asyncApi.createKeyPage(createKeyPage, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID updateKeyPage(final UpdateKeyPage updateKeyPage, final Principal... principal) {
        try {
            return asyncApi.updateKeyPage(updateKeyPage, principal).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID updateKey(final UpdateKey updateKey, final Principal... signers) {
        try {
            return asyncApi.updateKey(updateKey, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TransactionQueryResult getTx(final TxID txId) {
        return getTx(txId.getHash());
    }

    public TransactionQueryResult getTx(final byte[] txId) {
        return getTx(new TxnQuery().txid(txId));
    }

    public TransactionQueryResult getTx(final TxnQuery txnQuery) {
        try {
            return asyncApi.getTx(txnQuery).join();
        } catch (CompletionException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            }
            throw new RuntimeException(e.getCause());
        }
    }

    public List<TransactionQueryResult> getTxHistory(final TxHistoryQuery query) {
        return asyncApi.getTxHistory(query).join();
    }

    public ResponseKeyPageIndex queryKeyIndex(final KeyPageIndexQuery query) {
        try {
            return asyncApi.queryKeyIndex(query).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public DataEntryQueryResponse queryData(final DataEntryQuery query) {
        try {
            return asyncApi.queryData(query).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public MultiResponse<ResponseDataEntry> queryData(final DataEntrySetQuery query) {
        try {
            return asyncApi.queryData(query).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public ChainQueryResponse queryUrl(final GeneralQuery generalQuery) {
        try {
            return asyncApi.queryUrl(generalQuery).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public ChainQueryResponse queryChain(final ChainIdQuery chainIdQuery) {
        try {
            return asyncApi.queryChain(chainIdQuery).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public MultiResponse<MajorQueryResponse> queryMajorBlocks(final MajorBlocksQuery majorBlocksQuery) {
        try {
            return asyncApi.queryMajorBlocks(majorBlocksQuery).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public MultiResponse<MinorQueryResponse> queryMinorBlocks(final MinorBlocksQuery minorBlocksQuery) {
        try {
            return asyncApi.queryMinorBlocks(minorBlocksQuery).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public DataEntryQueryResponse queryDataSet(final DataEntrySetQuery dataEntrySetQuery) {
        try {
            return asyncApi.queryDataSet(dataEntrySetQuery).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public MultiResponse<Url> queryADIDirectory(final DirectoryQuery dataEntrySetQuery) {
        try {
            return asyncApi.queryADIDirectory(dataEntrySetQuery).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID addAuthority(final AddAccountAuthorityOperation addAccountAuthorityOperation, final Principal... signers) {
        try {
            return asyncApi.addAuthority(addAccountAuthorityOperation, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID removeAuthority(final RemoveAccountAuthorityOperation removeAccountAuthorityOperation, final Principal... signers) {
        try {
            return asyncApi.removeAuthority(removeAccountAuthorityOperation, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID disableAuthority(final DisableAccountAuthOperation disableAccountAuthorityOperation, final Principal... signers) {
        try {
            return asyncApi.disableAuthority(disableAccountAuthorityOperation, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID enableAuthority(
            final EnableAccountAuthOperation enableAccountAuthOperation, final Principal... signers) {
        try {
            return asyncApi.enableAuthority(enableAccountAuthOperation, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public TxID signTx(final Transaction tx, final Principal... signers) {
        try {
            return asyncApi.signTx(tx, signers).join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }

    public Long getOraclePrice() {
        try {
            return asyncApi.getOraclePrice().join();
        } catch (CompletionException e) {
            throw convertException(e);
        }
    }
}
