package io.accumulatenetwork.sdk.rpc;

public class NotFoundException extends RPCException {
    public NotFoundException(final int code, final String message) {
        super(code, message);
    }
}
