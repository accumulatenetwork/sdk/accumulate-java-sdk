package io.accumulatenetwork.sdk.rpc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.accumulatenetwork.sdk.generated.apiv2.RPCMethod;
import io.accumulatenetwork.sdk.rpc.models.RPCRequest;
import io.accumulatenetwork.sdk.rpc.models.RPCResponse;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

abstract class RPCClient {
    private static final Duration TIME_OUT = Duration.of(90, ChronoUnit.SECONDS); // TODO configurable

    protected static final Logger logger = Logger.getLogger(RPCClient.class.getName());
    private static final ObjectMapper objectMapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .configure(SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS, false);
    private static final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
    private static final Random random = new Random();

    private final URI uri;
    protected final HttpClient client = HttpClient.newHttpClient();
    protected final HttpResponse.BodyHandler<String> responseBodyHandler = HttpResponse.BodyHandlers.ofString();


    RPCClient() {
        final String apiEndpoint = StringUtils.firstNonBlank(System.getProperty("accumulate.api"), System.getenv("ACC_API"));
        if (apiEndpoint == null) {
            throw new RuntimeException("The RPCClient() constructor needs either system property accumulate.api " +
                    "or environment variable ACC_API containing the Accumulate API endpoint");
        }
        try {
            this.uri = new URI(apiEndpoint);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    RPCClient(final URI uri) {
        this.uri = uri;
    }

    protected HttpRequest buildRequest(final Integer requestId, final RPCMethod rpcMethod, final Object body) throws JsonProcessingException {
        final JsonNode params = body != null ? objectMapper.convertValue(body, JsonNode.class) : null;
        final var rpcRequest = new RPCRequest("2.0", requestId, rpcMethod.getApiMethod(), params);
        final String requestJson = objectWriter.writeValueAsString(rpcRequest);
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, String.format("Request #%d to Accumulate:\r\n%s", requestId, requestJson));
        }

        final var request = HttpRequest.newBuilder()
                .uri(uri)
                .timeout(TIME_OUT)
                .POST(HttpRequest.BodyPublishers.ofString(requestJson, StandardCharsets.UTF_8))
                .build();
        return request;
    }

    protected RuntimeException buildRequestException(final Exception e) {
        return new RuntimeException(String.format("Posting the request to Accumulate endpoint %s failed", uri), e);
    }

    protected RuntimeException buildResponseException(final HttpResponse<String> response) {
        return new RuntimeException(
                String.format("HTTP error response from Accumulate endpoint %s, status code: %d, message: %s",
                        uri, response.statusCode(), response.body()));
    }

    protected Integer newRequestId() {
        return random.nextInt(5000); // TODO why 5000 in CLI?
    }

    protected RPCResponse sendInternalSync(final RPCMethod rpcMethod, final Object body) {
        try {
            final Integer requestId = newRequestId();
            final HttpRequest request = buildRequest(requestId, rpcMethod, body);
            final var response = client.send(request, responseBodyHandler);
            if (response.statusCode() < 200 || response.statusCode() > 202) {
                throw buildResponseException(response);
            }
            if (logger.isLoggable(Level.FINER)) {
                logger.log(Level.FINER, String.format("Response from Accumulate for request #%d:\r\n%s", requestId, response.body()));
            }
            return RPCResponse.from(response.body());
        } catch (final RPCException e) {
            throw e;
        } catch (final Exception e) {
            throw buildRequestException(e);
        }
    }
}
