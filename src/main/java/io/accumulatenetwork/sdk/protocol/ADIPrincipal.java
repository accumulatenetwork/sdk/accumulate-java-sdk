package io.accumulatenetwork.sdk.protocol;


import io.accumulatenetwork.sdk.generated.protocol.ADI;
import io.accumulatenetwork.sdk.generated.protocol.AccountType;

public class ADIPrincipal extends Principal {

    public ADIPrincipal() {
    }

    public ADIPrincipal(final String adiUrl, SignatureKeyPair keyPair) {
        super(new ADI().url(Url.toAccURL(adiUrl)), keyPair);
    }

    public ADIPrincipal(final Url adiUrl, SignatureKeyPair keyPair) {
        super(new ADI().url(adiUrl), keyPair);
    }

    public String exportToBase64() {
        return super.exportToBase64(AccountType.IDENTITY);
    }

    public static ADIPrincipal importFromBase64(final String adiUrl, final String data) {
        return importFromBase64(Url.toAccURL(adiUrl), data);
    }

    public static ADIPrincipal importFromBase64(final Url adiUrl, final String data) {
        final var keyPair = Principal.importKeyPairFromBase64(data);
        return new ADIPrincipal(adiUrl, keyPair);
    }
}
