package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.accumulatenetwork.sdk.generated.protocol.*;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreateIdentity.class, name = "createIdentity"),
        @JsonSubTypes.Type(value = CreateTokenAccount.class, name = "createTokenAccount"),
        @JsonSubTypes.Type(value = SendTokens.class, name = "sendTokens"),
        @JsonSubTypes.Type(value = CreateDataAccount.class, name = "createDataAccount"),
        @JsonSubTypes.Type(value = WriteData.class, name = "writeData"),
        @JsonSubTypes.Type(value = WriteDataTo.class, name = "writeDataTo"),
        @JsonSubTypes.Type(value = AcmeFaucet.class, name = "acmeFaucet"),
        @JsonSubTypes.Type(value = CreateToken.class, name = "createToken"),
        @JsonSubTypes.Type(value = IssueTokens.class, name = "issueTokens"),
        @JsonSubTypes.Type(value = BurnTokens.class, name = "burnTokens"),
        @JsonSubTypes.Type(value = CreateKeyPage.class, name = "createKeyPage"),
        @JsonSubTypes.Type(value = CreateKeyBook.class, name = "createKeyBook"),
        @JsonSubTypes.Type(value = AddCredits.class, name = "addCredits"),
        @JsonSubTypes.Type(value = UpdateKeyPage.class, name = "updateKeyPage"),
        @JsonSubTypes.Type(value = UpdateAccountAuth.class, name = "updateAccountAuth"),
        @JsonSubTypes.Type(value = UpdateKey.class, name = "updateKey"),
        @JsonSubTypes.Type(value = RemoteTransaction.class, name = "remote"),
        @JsonSubTypes.Type(value = SyntheticCreateIdentity.class, name = "syntheticCreateIdentity"),
        @JsonSubTypes.Type(value = SyntheticWriteData.class, name = "syntheticWriteData"),
        @JsonSubTypes.Type(value = SyntheticDepositTokens.class, name = "syntheticDepositTokens"),
        @JsonSubTypes.Type(value = SyntheticDepositCredits.class, name = "syntheticDepositCredits"),
        @JsonSubTypes.Type(value = SyntheticBurnTokens.class, name = "syntheticBurnTokens"),
        @JsonSubTypes.Type(value = SyntheticForwardTransaction.class, name = "syntheticForwardTransaction"),
        @JsonSubTypes.Type(value = SystemGenesis.class, name = "systemGenesis"),
        @JsonSubTypes.Type(value = SystemWriteData.class, name = "systemWriteData"),
        @JsonSubTypes.Type(value = BlockValidatorAnchor.class, name = "blockValidatorAnchor"),
        @JsonSubTypes.Type(value = DirectoryAnchor.class, name = "directoryAnchor"),
})
public interface TransactionBody extends Marshallable {
}
