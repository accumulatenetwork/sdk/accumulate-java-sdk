package io.accumulatenetwork.sdk.protocol;


import io.accumulatenetwork.sdk.generated.protocol.AccountType;
import io.accumulatenetwork.sdk.generated.protocol.LiteTokenAccount;
import io.accumulatenetwork.sdk.generated.protocol.SignatureType;
import io.accumulatenetwork.sdk.signing.AccKeyPairGenerator;

public class LiteTokenAccountPrincipal extends Principal {

    private static final UrlRegistry urlRegistry = new UrlRegistry();

    public LiteTokenAccountPrincipal() {
    }


    public LiteTokenAccountPrincipal(SignatureKeyPair keyPair) {
        super(new LiteTokenAccount()
                .tokenUrl(urlRegistry.getAcmeTokenUrl())
                .url(computeUrl(keyPair.getPublicKey(), urlRegistry.getAcmeTokenUrl())), keyPair);
    }

    public LiteTokenAccountPrincipal(Url acmeTokenUrl, SignatureKeyPair keyPair) {
        super(new LiteTokenAccount().tokenUrl(Url.toAccURL(acmeTokenUrl.string())), keyPair);
    }

    /**
     * Generate a new random LiteAccount for the ACME token
     */
    public static LiteTokenAccountPrincipal generate(final SignatureType signatureType) {
        final SignatureKeyPair keyPair = new SignatureKeyPair(AccKeyPairGenerator.generate(signatureType), signatureType);
        return new LiteTokenAccountPrincipal(keyPair);
    }

    /**
     * Generate a new LiteAccount for the ACME token with the given keypair
     */
    static LiteTokenAccountPrincipal generateWithKeypair(final SignatureKeyPair signatureKeyPair) {
        return new LiteTokenAccountPrincipal(urlRegistry.getAcmeTokenUrl(), signatureKeyPair);
    }

    /**
     * Generate a new random LiteAccount for the given token URL
     */
    static LiteTokenAccountPrincipal generateWithTokenUrl(final Url tokenUrl, final SignatureType signatureType) {
        final SignatureKeyPair keyPair = new SignatureKeyPair(AccKeyPairGenerator.generate(signatureType), signatureType);
        return new LiteTokenAccountPrincipal(tokenUrl, keyPair);
    }

    static LiteTokenAccountPrincipal generateWithTokenUrl(final String tokenUrl, final SignatureType signatureType) {
        return generateWithTokenUrl(Url.toAccURL(tokenUrl), signatureType);
    }

    public String exportToBase64() {
        return super.exportToBase64(AccountType.LITE_TOKEN_ACCOUNT);
    }

    public static LiteTokenAccountPrincipal importFromBase64(final String data) {
        final var keyPair = Principal.importKeyPairFromBase64(data);
        return new LiteTokenAccountPrincipal(keyPair);
    }
}
