package io.accumulatenetwork.sdk.protocol;

import io.accumulatenetwork.sdk.generated.protocol.Envelope;
import io.accumulatenetwork.sdk.generated.protocol.Transaction;
import io.accumulatenetwork.sdk.generated.protocol.TransactionHeader;
import io.accumulatenetwork.sdk.signing.SignersPreparer;
import net.jodah.expiringmap.ExpiringMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class EnvelopeBuilder {
    private static final Map<Url, PrincipalContext> principalLockMap = Collections.synchronizedMap(ExpiringMap.builder()
            .maxSize(2 ^ 24) // We should never get this high, but it's just a measure to not make it infinite and allow it to fill up the heap
            .expiration(10, TimeUnit.MINUTES)
            .build());

    private final Envelope envelope;
    private PrincipalContext principalContext;

    public EnvelopeBuilder(final Url principalUrl, final SignersPreparer signersPreparer, final TransactionBody body) {
        try {
            lockPrincipal(principalUrl);
            envelope = new Envelope();

            final Transaction tx = new Transaction()
                    .header(new TransactionHeader()
                            .principal(principalUrl))
                    .body(body);
            envelope.setTransaction(new Transaction[]{tx});

            final List<Signature> signers = new ArrayList<>();
            final AtomicReference<byte[]> txHash = new AtomicReference<>();
            signersPreparer.prepareSigners().forEach(signer -> {
                if (signers.isEmpty()) {
                    final var signExecutor = signer.initiate(tx);
                    signers.add(signExecutor.getModel());
                    txHash.set(signExecutor.getTransactionHash());
                } else {
                    final var signExecutor = signer.signAdditional(txHash.get());
                    signers.add(signExecutor.getModel());
                }
            });
            envelope.setSignatures(signers.toArray(signers.toArray(new Signature[0])));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    // For signing existing / pending transactions
    public EnvelopeBuilder(final Url principalUrl, final SignersPreparer signersPreparer, final Transaction tx) {
        try {
            lockPrincipal(principalUrl);
            envelope = new Envelope();
            final Transaction hashedTx;
            if (tx.gethash() != null) {
                hashedTx = tx;
            } else {
                hashedTx = TransactionHasher.hashTransaction(tx);
            }

            envelope.setTransaction(new Transaction[]{hashedTx});

            final List<Signature> signers = new ArrayList<>();
            signersPreparer.prepareSigners().forEach(signer -> {
                final var signExecutor = signer.signAdditional(hashedTx.gethash());
                signers.add(signExecutor.getModel());
            });
            envelope.setSignatures(signers.toArray(signers.toArray(new Signature[0])));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void lockPrincipal(final Url principalUrl) throws InterruptedException {
        final PrincipalContext context = Optional.ofNullable(principalLockMap.get(principalUrl)).orElseGet(() -> {
            final var newContext = new PrincipalContext();
            principalLockMap.put(principalUrl, newContext);
            return newContext;
        });
        context.semaphore.acquire();
        context.start();
        this.principalContext = context;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public void releasePrincipal() {
        if (principalContext == null) {
            throw new IllegalArgumentException("the PrincipalContext context is null, this envelope may have been released already or the context expired");
        }
        principalContext.semaphore.release();
        principalContext.end();
    }


    private class PrincipalContext {
        private final Semaphore semaphore = new Semaphore(1);
        private long startTime;
        private Long lastUsedTime;

        private void start() throws InterruptedException {
            this.startTime = System.currentTimeMillis();

            // We want the envelopes signed at least 1ms apart, Accumulate does not accept duplicate timestamp nonces for the same principal.
            // On a mid-end CPU this routine will take ~1.5ms anyway, but a high-end CPU will outperform 1ms.
            // (TODO this could be refined by having the lock & lastSignatureTime per principal)
            if (lastUsedTime != null && startTime - lastUsedTime <= 1) {
                Thread.sleep(1);
            }
        }

        public void end() {
            this.lastUsedTime = startTime;
        }
    }
}
