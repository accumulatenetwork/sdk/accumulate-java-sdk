package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.accumulatenetwork.sdk.generated.protocol.BlockValidatorAnchor;
import io.accumulatenetwork.sdk.generated.protocol.DirectoryAnchor;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BlockValidatorAnchor.class, name = "blockValidatorAnchor"),
        @JsonSubTypes.Type(value = DirectoryAnchor.class, name = "directoryAnchor")})
public interface AnchorBody {
}
