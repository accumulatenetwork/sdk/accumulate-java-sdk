package io.accumulatenetwork.sdk.protocol;

import java.nio.charset.StandardCharsets;

public class FactomExtRef {

    private final byte[] data;

    public FactomExtRef(final byte[] data) {
        this.data = data;
    }

    public FactomExtRef(final String data) {
        if (data != null) {
            this.data = data.getBytes(StandardCharsets.UTF_8);
        } else {
            this.data = null;
        }
    }

    public byte[] getData() {
        return data;
    }
}
