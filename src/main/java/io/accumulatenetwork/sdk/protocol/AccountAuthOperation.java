package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.accumulatenetwork.sdk.generated.protocol.AddAccountAuthorityOperation;
import io.accumulatenetwork.sdk.generated.protocol.DisableAccountAuthOperation;
import io.accumulatenetwork.sdk.generated.protocol.EnableAccountAuthOperation;
import io.accumulatenetwork.sdk.generated.protocol.RemoveAccountAuthorityOperation;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({ // See enum AccountAuthOperationType
        @JsonSubTypes.Type(value = EnableAccountAuthOperation.class, name = "enable"),
        @JsonSubTypes.Type(value = DisableAccountAuthOperation.class, name = "disable"),
        @JsonSubTypes.Type(value = AddAccountAuthorityOperation.class, name = "addAuthority"),
        @JsonSubTypes.Type(value = RemoveAccountAuthorityOperation.class, name = "removeAuthority"),
})
public interface AccountAuthOperation extends Marshallable {
}
