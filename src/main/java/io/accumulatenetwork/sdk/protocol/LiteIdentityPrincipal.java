package io.accumulatenetwork.sdk.protocol;


import io.accumulatenetwork.sdk.generated.protocol.AccountType;
import io.accumulatenetwork.sdk.generated.protocol.LiteIdentity;
import io.accumulatenetwork.sdk.generated.protocol.SignatureType;
import io.accumulatenetwork.sdk.signing.AccKeyPairGenerator;

public class LiteIdentityPrincipal extends Principal {

    public LiteIdentityPrincipal() {
    }

    public LiteIdentityPrincipal(SignatureKeyPair keyPair) {
        super(new LiteIdentity()
                .url(computeUrl(keyPair.getPublicKey())), keyPair);
    }

    /**
     * Generate a new random LiteAccount for the ACME token
     */
    public static LiteIdentityPrincipal generate(final SignatureType signatureType) {
        final SignatureKeyPair keyPair = new SignatureKeyPair(AccKeyPairGenerator.generate(signatureType), signatureType);
        return new LiteIdentityPrincipal(keyPair);
    }

    /**
     * Generate a new LiteAccount for the ACME token with the given keypair
     */
    static LiteIdentityPrincipal generateWithKeypair(final SignatureKeyPair signatureKeyPair) {
        return new LiteIdentityPrincipal(signatureKeyPair);
    }


    public String exportToBase64() {
        return super.exportToBase64(AccountType.LITE_IDENTITY);
    }

    public static LiteIdentityPrincipal importFromBase64(final String data) {
        final var keyPair = Principal.importKeyPairFromBase64(data);
        return new LiteIdentityPrincipal(keyPair);
    }
}
