package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class MultiResponse<T> {
    private List<T> items;
    private List<JsonNode> otherItems;
    private long start;
    private long count;
    private long total;


    public MultiResponse() {
    }

    public MultiResponse(final List<T> items, final List<JsonNode> otherItems, final long start, final long count, final long total) {
        this.items = items;
        this.otherItems = otherItems;
        this.start = start;
        this.count = count;
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(final List<T> items) {
        this.items = items;
    }

    public List<JsonNode> getOtherItems() {
        return otherItems;
    }

    public void setOtherItems(final List<JsonNode> otherItems) {
        this.otherItems = otherItems;
    }

    public long getStart() {
        return start;
    }

    public void setStart(final long start) {
        this.start = start;
    }

    public long getCount() {
        return count;
    }

    public void setCount(final long count) {
        this.count = count;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(final long total) {
        this.total = total;
    }
}
