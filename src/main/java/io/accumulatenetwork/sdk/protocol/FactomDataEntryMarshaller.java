package io.accumulatenetwork.sdk.protocol;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public abstract class FactomDataEntryMarshaller implements DataEntry {

    public abstract byte[] getAccountId();

    public abstract byte[][] getExtIds();

    public abstract byte[] getData();

    @Override
    public byte[] marshalBinary() {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final var output = new DataOutputStream(bos);
        try {
            // Header, version byte 0x00
            output.writeByte(0);
            output.write(getAccountId());
            final byte[] extRefsBuf = marshallExtIds();
            output.writeShort(extRefsBuf.length);
            output.write(extRefsBuf);
            final byte[] data = getData();
            if(data != null) {
                output.write(data);
            }
            return bos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] marshallExtIds() {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final var output = new DataOutputStream(bos);
        try {
            for (final byte[] extId : getExtIds()) {
                output.writeShort(extId.length);
                output.write(extId);
            }
            return bos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
