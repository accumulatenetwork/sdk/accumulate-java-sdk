package io.accumulatenetwork.sdk.protocol;

public interface IntValueEnum {
    int getValue();
}
