package io.accumulatenetwork.sdk.protocol;

import com.iwebpp.crypto.TweetNaclFast;
import io.accumulatenetwork.sdk.commons.codec.binary.Hex;
import io.accumulatenetwork.sdk.generated.protocol.AccountType;
import io.accumulatenetwork.sdk.generated.protocol.SignatureType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import static io.accumulatenetwork.sdk.support.HashUtils.sha256;

public class Principal {

    private Account account;
    private SignatureKeyPair signatureKeyPair;

    private int signerVersion = 1;

    public Principal(final Account account, final SignatureKeyPair signatureKeyPair) {
        this.account = account;
        this.signatureKeyPair = signatureKeyPair;
    }

    public Principal(final Account account, final SignatureKeyPair signatureKeyPair, final int signerVersion) {
        this.account = account;
        this.signatureKeyPair = signatureKeyPair;
        this.signerVersion = signerVersion;
    }

    public Principal() {
    }

    public Account getAccount() {
        return account;
    }

    public SignatureKeyPair getSignatureKeyPair() {
        return signatureKeyPair;
    }

    public int getSignerVersion() {
        return signerVersion;
    }

    public void setSignerVersion(final int signerVersion) {
        this.signerVersion = signerVersion;
    }

    protected String exportToBase64(final AccountType accountType) {
        try {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final DataOutputStream outputStream = new DataOutputStream(byteArrayOutputStream);
            outputStream.writeByte(accountType.ordinal());
            outputStream.writeByte(this.getSignatureKeyPair().getSignatureType().ordinal());
            final byte[] privateKey = this.getSignatureKeyPair().getPrivateKey();
            outputStream.writeByte(privateKey.length);
            outputStream.write(privateKey);
            return Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected static SignatureKeyPair importKeyPairFromBase64(final String data) {
        try {
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(Base64.getDecoder().decode(data));
            final DataInputStream inputStream = new DataInputStream(byteArrayInputStream);
            inputStream.skipBytes(1); // We don't use the account type atm
            final SignatureType signatureType = SignatureType.fromValue(inputStream.readByte());
            final byte[] secretKey = new byte[inputStream.readByte()];
            inputStream.read(secretKey);
            return new SignatureKeyPair(TweetNaclFast.Signature.keyPair_fromSecretKey(secretKey), signatureType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected static Url computeUrl(final byte[] publicKey) {
        return computeUrl(publicKey, null);
    }

    protected static Url computeUrl(final byte[] publicKey, final Url mergeUrl) {
        final byte[] hash = sha256(publicKey);
        final byte[] copy = Arrays.copyOfRange(hash, 0, 20);
        final String pkHash = Hex.encodeHexString(copy);
        final byte[] checkSum = sha256(pkHash.getBytes());
        final byte[] checksumCopy = Arrays.copyOfRange(checkSum, 28, 32);
        final String checkSumStr = Hex.encodeHexString(checksumCopy);
        final var urlBuilder = new StringBuilder().append("acc://").append(pkHash).append(checkSumStr);
        if (mergeUrl != null) {
            urlBuilder.append("/").append(mergeUrl.authority()).toString();
        }
        return Url.toAccURL(urlBuilder.toString());
    }


}
