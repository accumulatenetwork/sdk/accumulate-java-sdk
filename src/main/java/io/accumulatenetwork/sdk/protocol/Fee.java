package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import java.math.BigInteger;

public class Fee {

    public Fee() {
    }

    public Fee(final BigInteger value) {
        this.value = value;
    }


    @JsonUnwrapped // TODO Test if this is working
    private BigInteger value;

    public BigInteger getValue() {
        return value;
    }

    public void setValue(final BigInteger value) {
        this.value = value;
    }
}
