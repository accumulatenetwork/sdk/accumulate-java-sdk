package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.accumulatenetwork.sdk.generated.protocol.*;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ED25519Signature.class, name = "ed25519"),
        @JsonSubTypes.Type(value = RCD1Signature.class, name = "rcd1"),
        @JsonSubTypes.Type(value = ReceiptSignature.class, name = "receipt"),
        @JsonSubTypes.Type(value = PartitionSignature.class, name = "partition"),
        @JsonSubTypes.Type(value = InternalSignature.class, name = "internal"),
        @JsonSubTypes.Type(value = AuthoritySignature.class, name = "authority"),
})
public interface Signature {
}
