package io.accumulatenetwork.sdk.protocol;

public enum InitHashMode {
    INIT_WITH_MERKLE_HASH, INIT_WITH_SIMPLE_HASH
}
