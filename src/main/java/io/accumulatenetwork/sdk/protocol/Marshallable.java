package io.accumulatenetwork.sdk.protocol;

public interface Marshallable extends RPCBody {
    byte[] marshalBinary();
}
