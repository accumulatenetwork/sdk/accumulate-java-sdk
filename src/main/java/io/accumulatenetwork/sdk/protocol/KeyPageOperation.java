package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.accumulatenetwork.sdk.generated.protocol.*;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({ // See enum KeyPageOperationType
        @JsonSubTypes.Type(value = AddKeyOperation.class, name = "add"),
        @JsonSubTypes.Type(value = UpdateKeyOperation.class, name = "update"),
        @JsonSubTypes.Type(value = RemoveKeyOperation.class, name = "remove"),
        @JsonSubTypes.Type(value = SetThresholdKeyPageOperation.class, name = "setThreshold"),
        @JsonSubTypes.Type(value = UpdateAllowedKeyPageOperation.class, name = "updateAllowed"),
})
public interface KeyPageOperation extends Marshallable {
}
