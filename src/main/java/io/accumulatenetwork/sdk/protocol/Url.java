package io.accumulatenetwork.sdk.protocol;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.split;

public class Url {

    private URI url;


    @JsonValue
    public URI getUrl() {
        return url;
    }

    Url() { // This allows Jackson the construct this using reflection
    }

    public Url(String value) throws MalformedURLException, URISyntaxException {
        final var url = new URI(value);
        if (url.getHost().isEmpty()) {
            throw new Error("Missing authority");
        }
        this.url = url;
    }

    public Url(URI url) {
        if (url.getHost() == null || url.getHost().isEmpty()) {
            throw new Error("Missing authority");
        }
        this.url = url;
    }

    public static Url toAccURL(String arg) {
        try {
            return Url.parse(arg);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Parse a string into an AccURL
     */
    public static Url parse(String str) throws URISyntaxException {
        if (!str.contains("://")) {
            str = "acc://" + str;
        }
        URI uri = new URI(str);
        return new Url(uri);
    }

    public String authority() {
        return this.url.getHost();
    }

    public String hostName() {
        if (isNotEmpty(url.getHost())) {
            final String[] split = split(url.getHost(), ':');
            return split[0];
        }
        return null;
    }

    public String path() {
        return StringUtils.stripEnd(this.url.getPath(), "/");
    }

    public String query() {
        return this.url.getQuery();
    }

    public String fragment() {
        return this.url.getAuthority();
    }

    public String string() {
        return this.url.toString();
    }

    public Url rootUrl() {
        return toAccURL(url.getScheme() + "://" + url.getHost());
    }

    public Url parentUrl() {
        String path = path();
        if (StringUtils.isEmpty(path)) {
            throw new IllegalArgumentException("this is already the URL root");
        }
        final int cutPos = path.lastIndexOf('/');
        if (cutPos > -1) {
            path = path.substring(0, cutPos);
        }
        return toAccURL(url.getScheme() + "://" + url.getHost() + path);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Url url1 = (Url) o;
        return Objects.equals(url, url1.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }

    @Override
    public String toString() {
        return string();
    }


    @JsonCreator
    public static Url fromJsonNode(final JsonNode jsonNode) {
        if (jsonNode.isTextual()) {
            return Url.toAccURL(jsonNode.asText());
        }
        throw new RuntimeException(String.format("'%s' is not a valid Url", jsonNode.toPrettyString()));
    }

}
