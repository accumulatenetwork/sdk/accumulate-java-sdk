package io.accumulatenetwork.sdk.protocol;

import io.accumulatenetwork.sdk.commons.codec.binary.Hex;
import io.accumulatenetwork.sdk.support.HashBuilder;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FactomEntry {

    private final byte[] data;

    private List<FactomExtRef> extRefs = new ArrayList<>();


    public FactomEntry() {
        this.data = null;
    }

    public FactomEntry(final byte[] data) {
        this.data = data;
    }

    public FactomEntry(final String data) {
        if (data != null) {
            this.data = data.getBytes(StandardCharsets.UTF_8);
        } else {
            this.data = null;
        }
    }

    public byte[] getData() {
        return data;
    }

    public List<FactomExtRef> getExtRefs() {
        return extRefs;
    }

    public FactomEntry addExtRef(final byte[] data) {
        getExtRefs().add(new FactomExtRef(data));
        return this;
    }

    public FactomEntry addExtRef(final String data) {
        getExtRefs().add(new FactomExtRef(data));
        return this;
    }

    public FactomEntry addExtRef(final FactomExtRef extRef) {
        getExtRefs().add(extRef);
        return this;
    }

    public byte[] calculateChainId() {
        final var hashBuilder = new HashBuilder();
        getExtRefs().forEach(extRef -> hashBuilder.addBytes(extRef.getData()));
        final byte[] chainId = new byte[32];
        System.arraycopy(hashBuilder.getCheckSum(), 0, chainId, 0, 32);
        return chainId;
    }

    public Url toUrl() {
        return Url.toAccURL(Hex.encodeHexString(calculateChainId()));
    }
}
