package io.accumulatenetwork.sdk.tests;

import io.accumulatenetwork.sdk.api.v2.AccumulateSyncApi;
import io.accumulatenetwork.sdk.api.v2.TransactionQueryResult;
import io.accumulatenetwork.sdk.generated.apiv2.*;
import io.accumulatenetwork.sdk.generated.errors.Status;
import io.accumulatenetwork.sdk.generated.protocol.KeyPage;
import io.accumulatenetwork.sdk.generated.protocol.*;
import io.accumulatenetwork.sdk.protocol.*;
import io.accumulatenetwork.sdk.signing.AccKeyPairGenerator;
import io.accumulatenetwork.sdk.support.ResultReader;
import io.accumulatenetwork.sdk.support.Retry;
import org.junit.jupiter.api.*;

import java.math.BigInteger;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class E2ETest extends AbstractTestBase {

    private final AccumulateSyncApi accumulate = new AccumulateSyncApi();
    private TxID queryTxnTxId;
    private TxID dataAccountTxId;

    private byte[] queryKeyIndexKey;
    private static SignatureKeyPair pageAddKey;

    @Test
    @Order(0)
    public void testDescribe() {
        final var response = accumulate.describe();
        System.out.println(response);
    }

    @Test
    @Order(1)
    public void testFaucet() {
        final var txId = accumulate.faucet(liteAccount.getAccount().getUrl());
        try {
            waitForTx(txId);
        } catch (Exception e) {
            // Ignore "not enough synthetic TXs" for now, it's due to a temp bug. Just wait a bit longer
            if (e.getMessage() == null || !e.getMessage().contains("not enough synthetic TXs")) {
                throw e;
            }
            try {
                Thread.sleep(8000);
            } catch (InterruptedException ie) {
            }
        }
    }

    @Test
    @Order(2)
    public void testGetLiteAccount() {
        waitForAnchor();
        final var response = accumulate.queryUrl(
                new GeneralQuery()
                        .url(liteAccount.getAccount().getUrl()));
        assertNotNull(response);
    }

    @Test
    @Order(3)
    public void testAddCredits() {
        waitForAnchor();
        final var addCredits = new AddCredits()
                .recipient(liteAccount.getAccount().getUrl())
                .amount(BigInteger.valueOf(11000000000000L / accumulate.getOraclePrice()));
        final var transactionResult = accumulate.addCredits(addCredits, liteAccount);
        final var addCreditsResult = transactionResult.getResult();
        assertNotNull(addCreditsResult);
        assertTrue(addCreditsResult.getCredits() > 0);
        queryTxnTxId = transactionResult.getTxID();
        final var txQueryResult = waitForTx(transactionResult.getTxID());
        assertEquals(txQueryResult.getTxType(), TransactionType.ADD_CREDITS);
        final AddCredits data = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), AddCredits.class);
        assertEquals(addCredits.getRecipient(), data.getRecipient());
        assertEquals(addCredits.getAmount(), data.getAmount());
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());

        var result = accumulate.getTxHistory(new TxHistoryQuery()
                .url(liteAccount.getAccount().getUrl())
                .start(1)
                .count(10));
        assertFalse(result.isEmpty());
    }

    @Test
    @Order(4)
    public void testGetHistory() {
        waitForAnchor();
        final var txn = new TxHistoryQuery()
                .url(liteAccount.getAccount().getUrl())
                .start(1)
                .count(10);
        var txnResult = accumulate.getTxHistory(txn);
        assertFalse(txnResult.isEmpty());
    }

    @Test
    @Order(5)
    public void testGetTransaction() {
        waitForAnchor();
        final var txn = new TxnQuery()
                .txid(queryTxnTxId.getHash());
        var txnResult = accumulate.getTx(txn);
        System.out.println(txnResult);
        assertNotNull(txnResult);
    }

    @Test
    @Order(6)
    public void testCreateIdentity() {
        waitForAnchor();
        final CreateIdentity createIdentity = new CreateIdentity()
                .url(rootADI)
                .keyHash(liteAccount.getSignatureKeyPair().getPublicKeyHash())
                .keyBookUrl(rootADI + "/book");
        var txId = accumulate.createIdentity(createIdentity, liteAccount);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_IDENTITY);
        final CreateIdentity data = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), CreateIdentity.class);
        assertEquals(createIdentity.getUrl(), data.getUrl());
        assertEquals(createIdentity.getKeyBookUrl(), data.getKeyBookUrl());
        assertArrayEquals(createIdentity.getKeyHash(), data.getKeyHash());
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(7)
    public void testGetADI() {
        waitForAnchor();
        final var response = accumulate.queryUrl(
                new GeneralQuery()
                        .url(rootADI));
        assertNotNull(response);
    }

    @Test
    @Order(8)
    public void testAddCreditsToADI() {
        accumulate.faucet(liteAccount.getAccount().getUrl());
        waitForAnchor();
        final String keyPageUrl = rootADI + "/book/1";
        final var addCredits = new AddCredits()
                .recipient(keyPageUrl)
                .amount(BigInteger.valueOf(30000000000000L / accumulate.getOraclePrice()));
        final var transactionResult = accumulate.addCredits(addCredits, liteAccount);
        final var addCreditsResult = transactionResult.getResult();
        System.out.println("Add Credits result");
        System.out.println(addCreditsResult.getCredits());
        assertNotNull(addCreditsResult);
        assertTrue(addCreditsResult.getCredits() > 0);
        final var txQueryResult = waitForTx(transactionResult.getTxID());
        assertEquals(txQueryResult.getTxType(), TransactionType.ADD_CREDITS);
        final AddCredits data = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), AddCredits.class);
        assertEquals(addCredits.getRecipient(), data.getRecipient());
        assertEquals(addCredits.getAmount(), data.getAmount());
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }


    @Test
    @Order(9)
    public void testCreateKeyBook() {
        waitForAnchor();
        final CreateKeyBook createKeyBook = new CreateKeyBook()
                .url(rootADI + "/testKeybook")
                .publicKeyHash(liteAccount.getSignatureKeyPair().getPublicKeyHash());
        final var adiPrincipal = new ADIPrincipal(rootADI, liteAccount.getSignatureKeyPair());
        var txId = accumulate.createKeyBook(createKeyBook, adiPrincipal);
        System.out.println("CreateKeyBook successful, TxID:" + txId);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_KEY_BOOK);
        final CreateKeyBook data = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), CreateKeyBook.class);
        assertEquals(createKeyBook.getUrl(), data.getUrl());
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(10)
    public void testCreateKeyPage() {
        waitForAnchor();
        KeySpecParams keySpecParams = new KeySpecParams();
        queryKeyIndexKey = AccKeyPairGenerator.generate(SignatureType.ED25519).getPublicKey();
        keySpecParams.setKeyHash(queryKeyIndexKey);
        KeySpecParams[] keySpecParams1 = {keySpecParams};
        final Principal adiPrincipal = new Principal(new KeyBook().url(rootADI + "/book"), liteAccount.getSignatureKeyPair());
        final CreateKeyPage createKeyPage = new CreateKeyPage().keys(keySpecParams1);
        var txId = accumulate.createKeyPage(createKeyPage, adiPrincipal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_KEY_PAGE);
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(11)
    public void testCreateDataAccount() {
        waitForAnchor();
        final Principal adiPrincipal = new Principal(new ADI().url(rootADI), liteAccount.getSignatureKeyPair());
        this.dataAccountTxId = accumulate.createDataAccount(new CreateDataAccount()
                .url(rootADI + "/data"), adiPrincipal);
        final var txQueryResult = waitForTx(dataAccountTxId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_DATA_ACCOUNT);
    }

    @Test
    @Order(12)
    public void testWriteData() {
        waitForAnchor();
        final Principal dataPrincipal = new Principal(new ADI().url(rootADI + "/data"), liteAccount.getSignatureKeyPair());
        var writeDataResult = accumulate.writeData(new WriteData()
                .entry(new AccumulateDataEntry()
                        .data(buildDataRecord("entry1", "entry2")))
                .scratch(true), dataPrincipal);
        assertNotNull(writeDataResult.getResult());
        waitForTx(writeDataResult.getTxID());
    }

    @Test
    @Order(13)
    public void testQueryData() {
        waitForAnchor();
        var result = accumulate.queryData(new DataEntryQuery()
                .url(rootADI + "/data"));
        assertNotNull(result);
    }

    @Test
    @Order(14)
    public void testQueryKeyIndex() {
        waitForAnchor();
        var result = accumulate.queryKeyIndex(
                new KeyPageIndexQuery()
                        .url(rootADI)
                        .key(queryKeyIndexKey));
        assertNotNull(result);
    }

    @Test
    @Order(15)
    public void createTokenAccountTest() {
        waitForAnchor();
        final CreateTokenAccount createTokenAccount = new CreateTokenAccount()
                .tokenUrl("acc://ACME")
                .url(rootADI + "/tokenAcc");
        final var adiPrincipal = new ADIPrincipal(rootADI, liteAccount.getSignatureKeyPair());
        final var txId = accumulate.createTokenAccount(createTokenAccount, adiPrincipal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_TOKEN_ACCOUNT);
    }

    @Test
    @Order(16)
    public void testCreateTokens() {
        waitForAnchor();
        final CreateToken createToken = new CreateToken()
                .url(rootADI + "/tokenIssuer")
                .symbol("ADT")
                .precision(2)
                .supplyLimit(BigInteger.valueOf(10000000000L));
        final var adiPrincipal = new ADIPrincipal(rootADI, liteAccount.getSignatureKeyPair());

        var txId = accumulate.createToken(createToken, adiPrincipal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_TOKEN);
    }

    @Test
    @Order(17)
    public void testSendTokens() {
        waitForAnchor();
        LiteTokenAccountPrincipal liteAccount1 = LiteTokenAccountPrincipal.generate(SignatureType.ED25519);
        final var txId = accumulate.faucet(liteAccount1.getAccount().getUrl());
        TokenRecipient[] tokenRecipient = {
                new TokenRecipient()
                        .url(liteAccount1.getAccount().getUrl())
                        .amount(BigInteger.valueOf(150L))
        };

        final SendTokens sendTokens = new SendTokens()
                .to(tokenRecipient);

        var txId1 = accumulate.sendTokens(sendTokens, liteAccount);
        System.out.println("Send tokens" + txId);
    }

    @Test
    @Order(18)
    public void testBurnTokens() {
        waitForAnchor();
        final BurnTokens burnTokens = new BurnTokens()
                .amount(BigInteger.valueOf(100L));
        var txId = accumulate.burnTokens(burnTokens, liteAccount);
        System.out.println("Burn tokens" + txId);
    }

    @Test
    @Order(19)
    public void testIssueTokens() {
        waitForAnchor();
        LiteTokenAccountPrincipal liteAccount1 = LiteTokenAccountPrincipal.generate(SignatureType.ED25519);
        final var txId = accumulate.faucet(liteAccount1.getAccount().getUrl());
        waitForAnchor();
        final IssueTokens issueTokens = new IssueTokens()
                .amount(BigInteger.valueOf(100L))
                .recipient(rootADI + "/tokenAcc");
        final var adiPrincipal = new ADIPrincipal(rootADI + "/tokenIssuer", liteAccount.getSignatureKeyPair());
        var txId1 = accumulate.issueTokens(issueTokens, adiPrincipal);
        System.out.println("Send tokens" + txId1);
    }

    @Test
    @Order(20)
    public void testMinorBlocks() {
        waitForAnchor();
        final MinorBlocksQuery minorBlocksQuery = new MinorBlocksQuery()
                .url("acc://bvn-bvn0")
                .count(10);
        var response = accumulate.queryMinorBlocks(minorBlocksQuery);
        assertFalse(response.getItems().isEmpty());
        System.out.println(response.getItems().get(0).getBlockTime());
    }

    @Test
    @Order(21)
    public void testQueryKeyBook() {
        waitForAnchor();
        final var response = accumulate.queryUrl(
                new GeneralQuery()
                        .url(rootADI + "/testKeybook"));
        assertNotNull(response);
    }

    @Test
    @Order(22)
    public void testQueryAdiDirectory() {
        waitForAnchor();
        final var response = accumulate.queryADIDirectory(
                new DirectoryQuery()
                        .url(rootADI)
                        .expand(true)
                        .count(10));
        assertNotNull(response);
        assertTrue(!response.getItems().isEmpty());
        assertTrue(!response.getOtherItems().isEmpty());
    }

    @Test
    @Order(23)
    public void testQueryDataSet() {
        waitForAnchor();
        var result = accumulate.queryDataSet(new DataEntrySetQuery()
                .url(rootADI + "/data")
                .count(10));
        assertNotNull(result);
    }

    @Test
    @Order(24)
    public void queryMajorBlock() {
        waitForAnchor();
        final MajorBlocksQuery minorBlocksQuery = new MajorBlocksQuery()
                .url("acc://bvn-bvn0")
                .count(10)
                .start(0);
        var response = accumulate.queryMajorBlocks(minorBlocksQuery);
        assertFalse(response.getItems().isEmpty());
        System.out.println(response.getItems().get(0).getMajorBlockTime());
        System.out.println("Response: " + response.getItems().get(0).getMajorBlockIndex());
    }

    @Test
    @Order(25)
    public void updateKeyPageAddKey() {
        waitForAnchor();
        this.pageAddKey = AccKeyPairGenerator.generateSignature(SignatureType.ED25519);
        final var addKeyOperation = new AddKeyOperation().entry(new KeySpecParams().keyHash(pageAddKey.getPublicKeyHash()));
        final var setThresholdOperation = new SetThresholdKeyPageOperation().threshold(2);
        final var keyPagePrincipal = new Principal(new KeyPage().url(rootADI + "/book/1"), liteAccount.getSignatureKeyPair());
        final KeyPageOperation[] keyPageOperation = {addKeyOperation, setThresholdOperation};
        final var updateKeyPage = new UpdateKeyPage().operation(keyPageOperation);
        final var txId = accumulate.updateKeyPage(updateKeyPage, keyPagePrincipal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.UPDATE_KEY_PAGE);
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(26)
    public void updateKeyPageRemoveKey() {
        waitForAnchor();
        final var removeKeyOperation = new RemoveKeyOperation().entry(new KeySpecParams().keyHash(this.pageAddKey.getPublicKeyHash()));
        final var keyPagePrincipal1 = new Principal(new KeyPage().url(rootADI + "/book/1"), liteAccount.getSignatureKeyPair(), 2);
        final KeyPageOperation[] keyPageOperation = {removeKeyOperation};
        final UpdateKeyPage updateKeyPage = new UpdateKeyPage().operation(keyPageOperation);
        final var txId = accumulate.updateKeyPage(updateKeyPage, keyPagePrincipal1);
        final var txQueryResult = accumulate.getTx(new TxnQuery()
                .txIdUrl(txId)
                .wait(Duration.ofSeconds(12)));
        assertEquals(txQueryResult.getTxType(), TransactionType.UPDATE_KEY_PAGE);
        assertEquals(Status.PENDING, txQueryResult.getQueryResponse().getStatus().getCode());

        // Get tx & sign
        final var keyPagePrincipal2 = new Principal(new KeyPage().url(rootADI + "/book/1"), this.pageAddKey, 2);
        final var signTxId = accumulate.signTx(txQueryResult.getQueryResponse().getTransaction(), keyPagePrincipal2);
        final var txSignResult = waitForTx(signTxId);
        assertEquals(Status.DELIVERED, txSignResult.getQueryResponse().getStatus().getCode());
    }


    @Test
    @Order(27)
    public void testSubADIWithRootKeybook() {
        final CreateIdentity createIdentity = new CreateIdentity()
                .url(rootADI + "/accounts")
                .authorities(new Url[]{Url.toAccURL(rootADI + "/book")});
        final Principal principal = new Principal(new ADI().url(rootADI), liteAccount.getSignatureKeyPair(), 3);
        var txId = accumulate.createIdentity(createIdentity, principal);
        waitForTx(txId);

        final CreateIdentity createIdentity2 = new CreateIdentity()
                .url(rootADI + "/accounts/sub1");
        final Principal principal2 = new Principal(new ADI().url(rootADI + "/accounts"), liteAccount.getSignatureKeyPair(), 3);
        accumulate.createIdentity(createIdentity2, principal2);
        waitForTx(txId);

        // Now create a token account, it should work without funding sub-no-book
        waitForAnchor();
        final CreateTokenAccount createTokenAccount = new CreateTokenAccount()
                .tokenUrl("acc://ACME")
                .url(rootADI + "/accounts/sub1/tokenAcc");
        final Principal tkPrincipal = new Principal(new ADI().url(rootADI + "/accounts/sub1"), liteAccount.getSignatureKeyPair(), 3);
        final var txIdTA = accumulate.createTokenAccount(createTokenAccount, tkPrincipal);
        final var txQueryResultTA = waitForTx(txId);

    }
/*
    // We can't test these without some BVN private keys

    @Test
    @Order(27)
    public void addAuthority() {
        waitForAnchor();
        final var addAccountAuthorityOperation = new AddAccountAuthorityOperation().authority(rootADI + "/testKeybook");
        final var principal = new Principal(new DataAccount().url(rootADI + "/data"), liteAccount.getSignatureKeyPair());
        final var txId = accumulate.addAuthority(addAccountAuthorityOperation, principal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.UPDATE_ACCOUNT_AUTH);
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(28)
    public void disableAuthority() {
        waitForAnchor();
        DisableAccountAuthOperation disableAccountAuthOperation = new DisableAccountAuthOperation().authority(rootADI + "/testKeybook");
        final Principal adiPrincipal = new Principal(new DataAccount().url(rootADI + "/data"), liteAccount.getSignatureKeyPair());
        var txId = accumulate.disableAuthority(disableAccountAuthOperation, adiPrincipal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.UPDATE_ACCOUNT_AUTH);
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(29)
    public void enableAuthority() {
        waitForAnchor();
        EnableAccountAuthOperation enableAccountAuthOperation = new EnableAccountAuthOperation().authority(rootADI + "/testKeybook");
        final Principal adiPrincipal = new Principal(new DataAccount().url(rootADI + "/data"), liteAccount.getSignatureKeyPair());
        var txId = accumulate.enableAuthority(enableAccountAuthOperation, adiPrincipal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.UPDATE_ACCOUNT_AUTH);
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(30)
    public void removeAuthority() {
        waitForAnchor();
        RemoveAccountAuthorityOperation removeAccountAuthorityOperation = new RemoveAccountAuthorityOperation().authority(rootADI + "/testKeybook");
        final Principal adiPrincipal = new Principal(new DataAccount().url(rootADI + "/data"), liteAccount.getSignatureKeyPair());
        var txId = accumulate.removeAuthority(removeAccountAuthorityOperation, adiPrincipal);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.UPDATE_ACCOUNT_AUTH);
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }
*/

    @Test
    @Order(99)
    public void updateKey() {
        waitForAnchor();
        final byte[] newKey = AccKeyPairGenerator.generate(SignatureType.ED25519).getPublicKey();
        final var keyPagePrincipal1 = new Principal(new KeyPage().url(rootADI + "/book/1"), liteAccount.getSignatureKeyPair(), 3);
        final var updateKey = new UpdateKey().newKeyHash(newKey);
        final var txId = accumulate.updateKey(updateKey, keyPagePrincipal1);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.UPDATE_KEY);
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    private TransactionQueryResult waitForTx(final TxID txId) {
        final var result = new AtomicReference<TransactionQueryResult>();
        final var txnQuery = new TxnQuery()
                .txid(txId.getHash())
                .wait(Duration.ofMinutes(1));
        new Retry()
                .withTimeout(1, ChronoUnit.MINUTES)
                .withDelay(2, ChronoUnit.SECONDS)
                .withMessage("")
                .execute(() -> {
                    final var txQueryResult = accumulate.getTx(txnQuery);
                    assertNotNull(txQueryResult);
                    final var queryResponse = txQueryResult.getQueryResponse();
                    if (queryResponse.getStatus().getCode() == Status.PENDING) {
                        return true;
                    }
                    if (!queryResponse.getType().equalsIgnoreCase("syntheticCreateIdentity")) { // TODO syntheticCreateIdentity returns CONFLICT?
                        assertEquals(Status.DELIVERED, queryResponse.getStatus().getCode());
                    }
                    if (queryResponse.getProduced() != null) {
                        for (var producedTxId : queryResponse.getProduced()) {
                            waitForTx(producedTxId);
                        }
                    }
                    result.set(txQueryResult);
                    return false;
                });
        return result.get();
    }
}
