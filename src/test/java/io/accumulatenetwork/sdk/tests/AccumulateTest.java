package io.accumulatenetwork.sdk.tests;

import io.accumulatenetwork.sdk.api.v2.AccumulateSyncApi;
import io.accumulatenetwork.sdk.api.v2.TransactionQueryResult;
import io.accumulatenetwork.sdk.api.v2.TransactionResult;
import io.accumulatenetwork.sdk.generated.apiv2.DataEntryQuery;
import io.accumulatenetwork.sdk.generated.apiv2.DataEntryQueryResponse;
import io.accumulatenetwork.sdk.generated.apiv2.DataEntrySetQuery;
import io.accumulatenetwork.sdk.generated.apiv2.TxHistoryQuery;
import io.accumulatenetwork.sdk.generated.apiv2.TxnQuery;
import io.accumulatenetwork.sdk.generated.errors.Status;
import io.accumulatenetwork.sdk.generated.protocol.ADI;
import io.accumulatenetwork.sdk.generated.protocol.AccumulateDataEntry;
import io.accumulatenetwork.sdk.generated.protocol.AddCredits;
import io.accumulatenetwork.sdk.generated.protocol.CreateDataAccount;
import io.accumulatenetwork.sdk.generated.protocol.CreateIdentity;
import io.accumulatenetwork.sdk.generated.protocol.ED25519Signature;
import io.accumulatenetwork.sdk.generated.protocol.TransactionType;
import io.accumulatenetwork.sdk.generated.protocol.WriteData;
import io.accumulatenetwork.sdk.generated.protocol.WriteDataResult;
import io.accumulatenetwork.sdk.generated.protocol.WriteDataTo;
import io.accumulatenetwork.sdk.generated.query.ResponseDataEntry;
import io.accumulatenetwork.sdk.protocol.ADIPrincipal;
import io.accumulatenetwork.sdk.protocol.FactomEntry;
import io.accumulatenetwork.sdk.protocol.LiteTokenAccountPrincipal;
import io.accumulatenetwork.sdk.protocol.MultiResponse;
import io.accumulatenetwork.sdk.protocol.Principal;
import io.accumulatenetwork.sdk.protocol.TxID;
import io.accumulatenetwork.sdk.support.ResultReader;
import io.accumulatenetwork.sdk.support.Retry;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle;


@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AccumulateTest extends AbstractTestBase {

    private AccumulateSyncApi accumulate = new AccumulateSyncApi();
    private TxID dataAccountTxId;

    @Test
    @Order(1)
    public void testFaucet() {
        final var txId = accumulate.faucet(liteAccount.getAccount().getUrl());
        waitForTx(txId);
    }

    @Test
    @Order(2)
    public void testAddCredits() {
        waitForAnchor();
        final var addCredits = new AddCredits()
                .recipient(liteAccount.getAccount().getUrl())
                .amount(BigInteger.valueOf(11000000000000L / accumulate.getOraclePrice()));
        final var transactionResult = accumulate.addCredits(addCredits, liteAccount);
        final var addCreditsResult = transactionResult.getResult();
        assertNotNull(addCreditsResult);
        assertTrue(addCreditsResult.getCredits() > 0);
        final var txQueryResult = waitForTx(transactionResult.getTxID());
        assertEquals(txQueryResult.getTxType(), TransactionType.ADD_CREDITS);
        final AddCredits data = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), AddCredits.class);
        assertEquals(addCredits.getRecipient(), data.getRecipient());
        assertEquals(addCredits.getAmount(), data.getAmount());
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());

        // TODO move to separate test
        var result = accumulate.getTxHistory(new TxHistoryQuery()
                .url(liteAccount.getAccount().getUrl())
                .start(1)
                .count(10));
        assertFalse(result.isEmpty());
    }


    @Test
    @Order(3)
    public void testCreateIdentity() {
        waitForAnchor();
        final CreateIdentity createIdentity = new CreateIdentity()
                .url(rootADI)
                .keyHash(liteAccount.getSignatureKeyPair().getPublicKeyHash())
                .keyBookUrl(rootADI + "/book");
        var txId = accumulate.createIdentity(createIdentity, liteAccount);
        System.out.println("CreateIdentity successful, TxID:" + txId);
        final var txQueryResult = waitForTx(txId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_IDENTITY);
        final CreateIdentity data = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), CreateIdentity.class);
        assertEquals(createIdentity.getUrl(), data.getUrl());
        assertEquals(createIdentity.getKeyBookUrl(), data.getKeyBookUrl());
        assertArrayEquals(createIdentity.getKeyHash(), data.getKeyHash());
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }

    @Test
    @Order(4)
    public void testAddCreditsToADI() {
        waitForAnchor();
        final String keyPageUrl = rootADI + "/book/1";
        final var addCredits = new AddCredits()
                .recipient(keyPageUrl)
                .amount(BigInteger.valueOf(300000000000L / accumulate.getOraclePrice()));
        final var transactionResult = accumulate.addCredits(addCredits, liteAccount);
        final var addCreditsResult = transactionResult.getResult();
        assertNotNull(addCreditsResult);
        assertTrue(addCreditsResult.getCredits() > 0);
        final var txQueryResult = waitForTx(transactionResult.getTxID());
        assertEquals(txQueryResult.getTxType(), TransactionType.ADD_CREDITS);
        final AddCredits data = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), AddCredits.class);
        assertEquals(addCredits.getRecipient(), data.getRecipient());
        assertEquals(addCredits.getAmount(), data.getAmount());
        assertEquals(Status.DELIVERED, txQueryResult.getQueryResponse().getStatus().getCode());
    }


    @Test
    @Order(5)
    public void testCreateDataAccount() {
        waitForAnchor();
        final Principal adiPrincipal = new Principal(new ADI().url(rootADI), liteAccount.getSignatureKeyPair());
        this.dataAccountTxId = accumulate.createDataAccount(new CreateDataAccount()
                .url(rootADI + "/data"), liteAccount);
        final var txQueryResult = waitForTx(dataAccountTxId);
        assertEquals(txQueryResult.getTxType(), TransactionType.CREATE_DATA_ACCOUNT);
    }

    @Test
    @Order(6)
    public void testWriteData() {
        waitForAnchor();
        final Principal dataPrincipal = new Principal(new ADI().url(rootADI + "/data"), liteAccount.getSignatureKeyPair());
        var writeDataResult = accumulate.writeData(new WriteData()
                .entry(new AccumulateDataEntry()
                        .data(buildDataRecord("entry1", "entry2")))
                .scratch(true), liteAccount);
        assertNotNull(writeDataResult.getResult());
        waitForTx(writeDataResult.getTxID());
    }

    @Test
    @Order(7)
    public void testQueryData() {
        waitForAnchor();
        var result = accumulate.queryData(new DataEntryQuery()
                .url(rootADI + "/data"));
        assertNotNull(result);
    }

    @Test
    @Order(8)
    public void testCreateLiteDataAccount() {
        waitForAnchor();
        var clonedLiteAccount = LiteTokenAccountPrincipal.importFromBase64(liteAccount.exportToBase64());
        TransactionResult<WriteDataResult> result = accumulate.createLiteDataAccount(new FactomEntry()
                .addExtRef("Factom PRO")
                .addExtRef("Tutorial"), liteAccount);
        var txQueryResult = waitForTx(result.getTxID());
        assertEquals(txQueryResult.getTxType(), TransactionType.WRITE_DATA_TO);
        final WriteDataTo resultData = ResultReader.readValue(txQueryResult.getQueryResponse().getData(), WriteDataTo.class);

        result = accumulate.writeFactomData(resultData.getRecipient().authority(), new FactomEntry("TheData".getBytes(StandardCharsets.UTF_8))
                .addExtRef("Extref1"), liteAccount);
        txQueryResult = waitForTx(result.getTxID());
        assertEquals(txQueryResult.getTxType(), TransactionType.WRITE_DATA_TO);

        queryFactomData(resultData);
    }

    private void queryFactomData(final WriteDataTo resultData) {
        MultiResponse<ResponseDataEntry> mr = accumulate.queryData(new DataEntrySetQuery()
                .url(resultData.getRecipient())
                .start(0)
                .count(1)
                .expand(false));
        mr = accumulate.queryData(new DataEntrySetQuery()
                .url(resultData.getRecipient())
                .start(mr.getTotal() - 2)
                .count(2)
                .expand(true));
        assertEquals(mr.getCount(), 2);
        final List<ResponseDataEntry> items = mr.getItems();
        assertEquals(items.size(), 2);
        assertNotNull(items.get(0).getEntry());
        assertNotNull(items.get(0).getTxId());
        assertNotNull(items.get(1).getEntry());

        final DataEntryQueryResponse dataEntryQueryResponse = accumulate.queryData(new DataEntryQuery()
                .url(resultData.getRecipient())
                .entryHash(items.get(1).getEntryHash()));
        assertNotNull(dataEntryQueryResponse.getEntry());
        assertNotNull(dataEntryQueryResponse.getEntryHash());


        final TransactionQueryResult txnResult = accumulate.getTx(new TxnQuery()
                .txIdUrl(items.get(0).getCauseTxId())
                .prove(true));

        // Check if we can get to the signature time
        assertTrue(Arrays.stream(txnResult.getQueryResponse().getSignatures())
                .findFirst()
                .map(signature -> {
                    if (signature instanceof ED25519Signature) {
                        final long epochMilli = ((ED25519Signature) signature).getTimestamp() / 1000;
                        final LocalDateTime signatureTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), ZoneId.systemDefault());
                        if (signatureTime.isBefore(LocalDateTime.now()) && signatureTime.isAfter(LocalDateTime.now().minusMinutes(2))) {
                            return true;
                        }
                    }
                    return null;
                }).isPresent());

        // Check if we can get to the block time
        assertNotNull(txnResult.getQueryResponse().getReceipts());
        assertTrue(
                Arrays.stream(txnResult.getQueryResponse().getReceipts())
                        .findFirst()
                        .map(txReceipt -> {
                            if (txReceipt.getLocalBlock() > 0) {
                                assertTrue(txReceipt.getLocalBlock() > 0);
                                final OffsetDateTime blockTime = txReceipt.getLocalBlockTime();
                                assertTrue(blockTime.isBefore(OffsetDateTime.now()) && blockTime.isAfter(OffsetDateTime.now().minusMinutes(2)));

                                return true;
                            }
                            return null;
                        })
                        .isPresent());
    }

    @Test
    @Order(9)
    public void testWriteDataTo() {
        waitForAnchor();
        final var adiPrincipal = new ADIPrincipal(rootADI + "/data", liteAccount.getSignatureKeyPair());
        var writeDataResult = accumulate.writeData(new WriteData()
                .entry(new AccumulateDataEntry()
                        .data(buildDataRecord("entry1", "entry2")))
                .scratch(true), adiPrincipal);
        assertNotNull(writeDataResult.getResult());
        waitForTx(writeDataResult.getTxID());
    }

    private TransactionQueryResult waitForTx(final TxID txId) {
        final var result = new AtomicReference<TransactionQueryResult>();
        final var txnQuery = new TxnQuery()
                .txid(txId.getHash())
                .wait(Duration.ofMinutes(1));
        new Retry()
                .withTimeout(1, ChronoUnit.MINUTES)
                .withDelay(2, ChronoUnit.SECONDS)
                .withMessage("")
                .execute(() -> {
                    final var txQueryResult = accumulate.getTx(txnQuery);
                    assertNotNull(txQueryResult);
                    final var queryResponse = txQueryResult.getQueryResponse();
                    if (queryResponse.getStatus().getCode() == Status.PENDING) {
                        return true;
                    }
                    if (!queryResponse.getType().equalsIgnoreCase("syntheticCreateIdentity")) { // TODO syntheticCreateIdentity returns CONFLICT?
                        assertEquals(Status.DELIVERED, queryResponse.getStatus().getCode());
                    }
                    if (queryResponse.getProduced() != null) {
                        for (var producedTxId : queryResponse.getProduced()) {
                            waitForTx(producedTxId);
                        }
                    }
                    result.set(txQueryResult);
                    return false;
                });
        return result.get();
    }
}
