package io.accumulatenetwork.sdk.examples.lite;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import com.iwebpp.crypto.TweetNaclFast.Signature;

import io.accumulatenetwork.sdk.api.v2.AccumulateSyncApi;
import io.accumulatenetwork.sdk.generated.apiv2.TransactionQueryResponse;
import io.accumulatenetwork.sdk.generated.errors.Status;
import io.accumulatenetwork.sdk.generated.protocol.AddCredits;
import io.accumulatenetwork.sdk.generated.protocol.SendTokens;
import io.accumulatenetwork.sdk.generated.protocol.SignatureType;
import io.accumulatenetwork.sdk.generated.protocol.TokenRecipient;
import io.accumulatenetwork.sdk.protocol.LiteTokenAccountPrincipal;
import io.accumulatenetwork.sdk.protocol.SignatureKeyPair;
import io.accumulatenetwork.sdk.protocol.TxID;
import io.accumulatenetwork.sdk.protocol.Url;
import io.accumulatenetwork.sdk.rpc.NotFoundException;

public class Program {
    private static AccumulateSyncApi accumulate;

    public static void main(String[] args) throws URISyntaxException, InterruptedException, MalformedURLException {
        accumulate = new AccumulateSyncApi(new URI("https://fozzie.accumulatenetwork.io/v2"));

        // Create the principal from a hard-coded key
        var principal = new LiteTokenAccountPrincipal(
            new SignatureKeyPair(
                Signature.keyPair_fromSecretKey(new byte[] {
                    (byte) 0xe4, (byte) 0x3b, (byte) 0xe9, (byte) 0x0e, (byte) 0x34, (byte) 0x92, (byte) 0x10, (byte) 0x45, (byte) 0x66, (byte) 0x62, (byte) 0xd8, (byte) 0xb8, (byte) 0xbd, (byte) 0xc9, (byte) 0xcc, (byte) 0x9e, (byte) 0x5e, (byte) 0x46, (byte) 0xcc, (byte) 0xb0, (byte) 0x7f, (byte) 0x21, (byte) 0x29, (byte) 0xe7, (byte) 0xb5, (byte) 0x7a, (byte) 0x81, (byte) 0x95, (byte) 0xe5, (byte) 0xe9, (byte) 0x16, (byte) 0xd5,
                    (byte) 0x52, (byte) 0x81, (byte) 0x3d, (byte) 0xbc, (byte) 0xd9, (byte) 0xe5, (byte) 0x4c, (byte) 0x8b, (byte) 0xf5, (byte) 0x04, (byte) 0xe8, (byte) 0x3b, (byte) 0x34, (byte) 0x61, (byte) 0x35, (byte) 0x5d, (byte) 0xf1, (byte) 0xf8, (byte) 0xed, (byte) 0x2f, (byte) 0x2e, (byte) 0x46, (byte) 0x6c, (byte) 0x7f, (byte) 0x30, (byte) 0xdf, (byte) 0xd3, (byte) 0x2c, (byte) 0xe7, (byte) 0xdb, (byte) 0xe7, (byte) 0x9e,
                }),
                SignatureType.ED25519
            )
        );

        // Faucet it
        waitFor(accumulate.faucet(principal.getAccount().getUrl()));
        
        // Get credits (1100.00)
        waitFor(accumulate.addCredits(
            new AddCredits()
                .recipient(principal.getAccount().getUrl())
                .amount(BigInteger.valueOf(11000000000000L / accumulate.getOraclePrice())),
            principal).getTxID());

        // Send tokens (0.0000015 ACME)
        waitFor(accumulate.sendTokens(
            new SendTokens().to(new TokenRecipient[] {
                new TokenRecipient()
                    .url(new Url("acc://df8381728d3798253f5aff9755924a152ce12e9b7e694ac9/ACME"))
                    .amount(BigInteger.valueOf(150L)),
            }),
            principal));
    }

    public static void waitFor(TxID id) throws InterruptedException {
        System.out.printf("Waiting for %s\n", id);
        while (true) {
            TransactionQueryResponse r;
            try {
                r = accumulate.getTx(id).getQueryResponse();
            } catch (NotFoundException e) {
                // Sleep if the transaction is not found
                Thread.sleep(1000);
                continue;
            }

            // Give up if the transaction failed
            if (r.getStatus().getCode().getValue() >= 400) {
                System.out.printf("%s failed\n", id);
                return;
            }

            // Sleep if the transaction is pending
            if (r.getStatus().getCode() == Status.PENDING) {
                Thread.sleep(1000);
                continue;
            }

            // Delivered, check produced
            if (r.getProduced() != null) {
                for (var produced : r.getProduced()) {
                    waitFor(produced);
                }
            }
            break;
        }
    }
}
